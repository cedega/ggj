#pragma once
#include "BuildMenu.hpp"
#include "Tower.hpp"

class UpgradeMenu
{
 public:
    UpgradeMenu();
    ~UpgradeMenu();

    void initialize(cl::Shader *image_shader);
    void update(cl::Camera *camera, const glm::vec3 &position, float dt);

    void show(Tower *tower);
    void cancel();
    void upgrade();
    bool isSelecting() const;

    cl::Image* getSelectedImage();
    Tower* getSelectedTower();
    cl::Text* getCostText();

 private:
    cl::Texture *upgrade_textures_[NUM_TOWERS];
    cl::Image upgrade_images_[NUM_TOWERS];
    cl::Font *cost_font_;
    cl::Text cost_text_;
    int selected_index_;
    bool selecting_;
    Tower *selected_tower_;
};
