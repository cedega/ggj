#pragma once

struct Buff {
    bool slow;
    bool dot;
    float tick;
    float length;
    float tickdmg;

    Buff() :
        slow(false),
        dot(false),
        tick(0.0f),
        length(0.0f),
        tickdmg(0.0f)
    {

    }
};
