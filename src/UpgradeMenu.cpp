#include "UpgradeMenu.hpp"

UpgradeMenu::UpgradeMenu() :
    selected_index_(0),
    selecting_(false),
    selected_tower_(NULL)
{

}

UpgradeMenu::~UpgradeMenu()
{
    for (int i = 0; i < NUM_TOWERS; i++)
        delete upgrade_textures_[i];

    delete cost_font_;
}

void UpgradeMenu::initialize(cl::Shader *billboard_shader)
{
    cl::AssetLoader loader;

    upgrade_textures_[WALL] = loader.loadTexture("data/ui/upgrades/1.png", cl::Loader::NONE, NULL);
    upgrade_textures_[ICE] = loader.loadTexture("data/ui/upgrades/2.png", cl::Loader::NONE, NULL);
    upgrade_textures_[POISON] = loader.loadTexture("data/ui/upgrades/3.png", cl::Loader::NONE, NULL);
    upgrade_textures_[FIRE] = loader.loadTexture("data/ui/upgrades/4.png", cl::Loader::NONE, NULL);
    upgrade_textures_[CRYSTAL] = loader.loadTexture("data/ui/upgrades/5.png", cl::Loader::NONE, NULL);
    upgrade_textures_[LIGHTNING] = loader.loadTexture("data/ui/upgrades/6.png", cl::Loader::NONE, NULL);

    upgrade_images_[0].initialize(upgrade_textures_[0], billboard_shader, false);
    upgrade_images_[1].initialize(upgrade_textures_[1], billboard_shader, false);
    upgrade_images_[2].initialize(upgrade_textures_[2], billboard_shader, false);
    upgrade_images_[3].initialize(upgrade_textures_[3], billboard_shader, false);
    upgrade_images_[4].initialize(upgrade_textures_[4], billboard_shader, false);
    upgrade_images_[5].initialize(upgrade_textures_[5], billboard_shader, false);

    cost_font_ = loader.loadFont("data/ui/font.ttf", cl::Loader::NONE, NULL, 48);
    cost_text_.initialize(cost_font_, billboard_shader, false);
    cost_text_.setScaling(glm::vec3(0.005f));
    cost_text_.setDepthTest(false);

    for (int i = 0; i < NUM_TOWERS; i++)
    {
        upgrade_images_[i].setScaling(glm::vec3(0.005f));
        upgrade_images_[i].setDepthTest(false);
    }
}

void UpgradeMenu::update(cl::Camera *camera, const glm::vec3 &position, float dt)
{
    for (int i = 0; i < NUM_TOWERS; i++)
    {
        upgrade_images_[i].updateLastPosition();
        upgrade_images_[i].setPosition(position, false);
    }

    glm::vec3 diff = glm::normalize(position - (camera->getPosition() + camera->getPositionOffset()));
    glm::vec3 cross = glm::cross(diff, glm::vec3(0.0f, 1.0f, 0.0f));

    cost_text_.updateLastPosition();
    cost_text_.setPosition(position + glm::vec3(0.4f * cross.x, -1.25f, 0.4f * cross.z), false);

    if (selected_tower_ == NULL)
        return;

    cost_text_.setString(cl::toStringI(selected_tower_->getCost()));
}

void UpgradeMenu::show(Tower *tower)
{
    selected_tower_ = tower; 
    selecting_ = true;
    selected_index_ = tower->getType();
}

void UpgradeMenu::cancel()
{
    selecting_ = false;
    selected_tower_ = NULL;
}

void UpgradeMenu::upgrade()
{
    selected_tower_->upgrade();
}

bool UpgradeMenu::isSelecting() const
{
    return selecting_;
}

cl::Image* UpgradeMenu::getSelectedImage()
{
    return &upgrade_images_[selected_index_];
}

Tower* UpgradeMenu::getSelectedTower()
{
    return selected_tower_;
}

cl::Text* UpgradeMenu::getCostText()
{
    return &cost_text_;
}

