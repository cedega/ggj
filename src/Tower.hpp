#pragma once
#include <Clone/3D/Entity3D.hpp>
#include <Clone/AssetLoader.hpp>
#include <Clone/GameSettings.hpp>
#include <Clone/Bullet/BulletPhysicsWorld.hpp>
#include <Clone/ParticleEmitter.hpp>
#include <iostream>
#include "Bullet.hpp"
#include "CollisionMask.hpp"
#include "Buff.hpp"
#include "SoundManager.hpp"
#include "BuildMenu.hpp"

class Player;

class Tower : public cl::Entity3D
{
 public:
    Tower();
    ~Tower();

    void uploadToShader(cl::Shader *shader, float interpolation, int draw_index);
    virtual void initialize(cl::GameSettings *gs, const std::string &tower_cat, cl::Shader *shader, cl::AssetCache *cache, cl::Shader *particle_shader, int type, cl::Shader *bullet_shader);

    void update(float dt);

    void kill();
    bool isAlive() const;
    bool destroy() const;
    void shoot(const glm::vec3 &location, const glm::vec3 &direction, cl::BulletPhysicsWorld *world);
    void upgrade();

    void setActive(bool active);
    bool isActive() const;

    int getType() const;

    int getLevel() const;
    float getCost() const;
    float getDamage() const;
    Buff getBuff() const;
    float getAttackRange() const;

    cl::BulletPhysicsObject* getPhysics();
    cl::ParticleEmitter* getParticles();
    std::vector<Bullet*>* getBullets();

 protected:
    bool alive_;
    bool active_;
    int type_;
    cl::GameSettings *settings_;
    std::string tower_cat_;
    cl::Shader *model_shader_;
    cl::Shader *particle_shader_;
    cl::Shader *bullet_shader_;
    cl::Armature armature_;
    btBoxShape physics_shape_;
    cl::BulletPhysicsObject physics_;
    cl::ParticleEmitter death_particles_;
    std::vector<Bullet*> bullets_;

    cl::SkeletalAnimation animation_;

    // Insert tower properties here
    int level_;
    float cost_;
    float damage_;
    Buff buff_;
    float mass_;

    float fire_tick_;
    float fire_time_;
    float aoe_;
    float range_;

    float cost_up_;
    float damage_up_;
    float length_up_;
    float tickdmg_up_;
    float range_up_;
    float rate_up_;
    float mass_up_;

};
