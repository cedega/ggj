#include "SoundManager.hpp"
#include "Player.hpp"

SoundManager* SoundManager::inst = 0;

SoundManager* SoundManager::instance() {
    if (inst == 0)
        inst = new SoundManager();
    return inst;
}

cl::Sound* SoundManager::at(unsigned int i) {
    if (i >= sound_.size())
        return 0;
    return sound_[i];
}

SoundManager::SoundManager() {
    cl::AssetLoader loader;
    // sound
    sound_.push_back(loader.loadSound("data/sound/death.ogg", 0, NULL));
    sound_.push_back(loader.loadSound("data/sound/slide.ogg", 0, NULL));
    sound_.push_back(loader.loadSound("data/sound/collect.ogg", 0, NULL));
    sound_.push_back(loader.loadSound("data/sound/fire_cast.ogg", 0, NULL));
    sound_.push_back(loader.loadSound("data/sound/burn.ogg", 0, NULL));
    sound_.push_back(loader.loadSound("data/sound/lightning_cast.ogg", 0, NULL));
    sound_.push_back(loader.loadSound("data/sound/electric.ogg", 0, NULL));
    sound_.push_back(loader.loadSound("data/sound/poison_cast.ogg", 0, NULL));
    sound_.push_back(loader.loadSound("data/sound/poison.ogg", 0, NULL));
    sound_.push_back(loader.loadSound("data/sound/cast_ice.ogg", 0, NULL));
    sound_.push_back(loader.loadSound("data/sound/ice.ogg", 0, NULL));
    sound_.push_back(loader.loadSound("data/sound/crystal.ogg", 0, NULL));
    sound_.push_back(loader.loadSound("data/sound/drop.ogg", 0, NULL));
    sound_.push_back(loader.loadSound("data/sound/spawn.ogg", 0, NULL));
    sound_.push_back(loader.loadSound("data/sound/girl_death_ogg.ogg", 0, NULL));
    sound_.push_back(loader.loadSound("data/sound/girl_hurt_ogg.ogg", 0, NULL));
    sound_.push_back(loader.loadSound("data/sound/charge.ogg", 0, NULL));
    sound_.push_back(loader.loadSound("data/sound/crumble.ogg", 0, NULL));
    
    for (unsigned int i = 0; i < sound_.size(); i++)
        sound_[i]->setVolume(25);
}

void SoundManager::setPlayer(Player* p) {
    player = p;
}
Player* SoundManager::getPlayer() {
    return player;
 }
