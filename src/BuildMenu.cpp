#include "BuildMenu.hpp"

BuildMenu::BuildMenu() :
    selected_index_(0),
    selecting_(false)
{

}

BuildMenu::~BuildMenu()
{
    for (int i = 0; i < NUM_TOWERS; i++)
        delete tower_textures_[i];

    delete cost_font_;
}

void BuildMenu::initialize(cl::Shader *billboard_shader)
{
    cl::AssetLoader loader;

    tower_textures_[WALL] = loader.loadTexture("data/ui/1.png", cl::Loader::NONE, NULL);
    tower_textures_[ICE] = loader.loadTexture("data/ui/2.png", cl::Loader::NONE, NULL);
    tower_textures_[POISON] = loader.loadTexture("data/ui/3.png", cl::Loader::NONE, NULL);
    tower_textures_[FIRE] = loader.loadTexture("data/ui/4.png", cl::Loader::NONE, NULL);
    tower_textures_[CRYSTAL] = loader.loadTexture("data/ui/5.png", cl::Loader::NONE, NULL);
    tower_textures_[LIGHTNING] = loader.loadTexture("data/ui/6.png", cl::Loader::NONE, NULL);

    tower_images_[0].initialize(tower_textures_[0], billboard_shader, false);
    tower_images_[1].initialize(tower_textures_[1], billboard_shader, false);
    tower_images_[2].initialize(tower_textures_[2], billboard_shader, false);
    tower_images_[3].initialize(tower_textures_[3], billboard_shader, false);
    tower_images_[4].initialize(tower_textures_[4], billboard_shader, false);
    tower_images_[5].initialize(tower_textures_[5], billboard_shader, false);

    cost_font_ = loader.loadFont("data/ui/font.ttf", cl::Loader::NONE, NULL, 48);
    cost_text_.initialize(cost_font_, billboard_shader, false);
    cost_text_.setScaling(glm::vec3(0.005f));
    cost_text_.setDepthTest(false);

    for (int i = 0; i < NUM_TOWERS; i++)
    {
        tower_images_[i].setScaling(glm::vec3(0.005f));
        tower_images_[i].setDepthTest(false);
    }
}

void BuildMenu::update(cl::Camera *camera, float cost, const glm::vec3 &position, float dt)
{
    for (int i = 0; i < NUM_TOWERS; i++)
    {
        tower_images_[i].updateLastPosition();
        tower_images_[i].setPosition(position, false);
    }

    glm::vec3 diff = glm::normalize(position - (camera->getPosition() + camera->getPositionOffset()));
    glm::vec3 cross = glm::cross(diff, glm::vec3(0.0f, 1.0f, 0.0f));

    cost_text_.updateLastPosition();
    cost_text_.setPosition(position + glm::vec3(0.4f * cross.x, -1.25f, 0.4f * cross.z), false);
    cost_text_.setString(cl::toStringI(cost));
}

void BuildMenu::next()
{
    selected_index_++;

    if (selected_index_ >= NUM_TOWERS)
        selected_index_ = 0;
}

void BuildMenu::previous()
{
    selected_index_--;

    if (selected_index_ < 0)
        selected_index_ = NUM_TOWERS - 1;
}

void BuildMenu::show()
{
    selecting_ = true;
}

void BuildMenu::cancel()
{
    selecting_ = false;
}

TOWERS BuildMenu::getSelection()
{
    return static_cast<TOWERS>(selected_index_);
}

bool BuildMenu::isSelecting() const
{
    return selecting_;
}

cl::Image* BuildMenu::getSelectedImage()
{
    return &tower_images_[selected_index_];
}

cl::Text* BuildMenu::getCostText()
{
    return &cost_text_;
}

