#include "Level.hpp" 

Level::Level() : ground_shape_(btVector3(0.0f, 1.0f, 0.0f), 0.0f),
    ground_(&ground_shape_, 0.0f, glm::quat(glm::vec3(0.0f)), glm::vec3(0.0f)),
    cur_wave_index_(0),
    finished_(false),
    finished_good_(false)
{
}
Level::~Level()
{
    for (unsigned int i = 0; i < towers_.size(); i++)
    {
        delete towers_[i];
    }

    for (unsigned int i = 0; i < enemies_.size(); i++)
    {
        delete enemies_[i];
    }
    for (unsigned int i = 0; i < waves_.size(); i++)
    {
        delete waves_[i];
    }
    for (unsigned int i = 0; i < ritualists_.size(); i++)
    {
        delete ritualists_[i];
    }

    for (unsigned int i = 0; i < crystals_.size(); i++)
    {
        delete crystals_[i];
    }
}

void Level::initialize(ShaderPack *shaders)
{
    // save shader
    sp = shaders;
    // Initialize Player

    player_.initialize(&towers_, &enemies_, &world_, &shaders->model_static, &shaders->image, &shaders->model_rigged, &shaders->billboard, &shaders->particles, &shaders->bullet);
    SoundManager *sm = SoundManager::instance();
    sm->setPlayer(&player_);
    // cultists
    ritualists_.push_back(new Ritualist(glm::vec3(-2.77912,2.825,4.97176)));
    ritualists_.push_back(new Ritualist(glm::vec3(-1.67635,2.825,3.08058)));
    ritualists_.push_back(new Ritualist(glm::vec3(1.51133,2.825,3.33619)));
    ritualists_.push_back(new Ritualist(glm::vec3(-1.88519,2.825,6.58313)));
    ritualists_.push_back(new Ritualist(glm::vec3(1.03916,2.825,6.33315)));

    for (unsigned int i = 0; i < ritualists_.size(); i++) {
        ritualists_[i]->initialize(&shaders->model_rigged);
        world_.addCharacter(ritualists_[i]->getPhysics(), COL_NONE, COL_NONE);
    }

    // Initialize Camera

    camera_.setProjectionMatrix(glm::perspective(glm::radians(35.0f), 1280.0f / 720.0f, 1.0f, 50.0f));
    camera_.snapTo(player_.getPosition());
    camera_.setLerpSpeed(4.0f);

    camera_2d_.setProjectionMatrix(glm::ortho(0.0f, 1280.0f, 720.0f, 0.0f, -1.0f, 1.0f));

    // Initialize Level Visuals

    cl::AssetLoader loader;
    level_model_data_ = loader.loadModelData("data/level/level.cm", cl::Loader::NO_NORMALS, NULL);
    level_model_.initialize(level_model_data_, &shaders->model_static);
    level_model_.setRotation(glm::vec3(-M_PI / 2.0f, 0.0f, 0.0f)); // Fuck Blender
    level_model_.setBackfaceCulling(false); // And fuck oli too for making backwards-facing mesh!!!

    // Initialize 2D UI
    font_ = loader.loadFont("data/ui/font.ttf", cl::Loader::NONE, &cache_2d_, 30);
    currency_text_.initialize(font_, &shaders->image, true);
    wave_text_.initialize(font_, &shaders->image, true);
    lives_text_.initialize(font_, &shaders->image, true);

    cl::Texture *currency_tex = loader.loadTexture("data/ui/counter.png", cl::Loader::NONE, &cache_2d_);
    currency_bg_.initialize(currency_tex, &shaders->image, true);
    currency_bg_.setTextureRect(glm::vec4(0, 35, 128, 56));

    cl::Texture *wave_tex = loader.loadTexture("data/ui/counter3.png", cl::Loader::NONE, &cache_2d_);
    wave_bg_.initialize(wave_tex, &shaders->image, true);
    wave_bg_.setTextureRect(glm::vec4(0, 35, 128, 56));

    cl::Texture *lives_tex = loader.loadTexture("data/ui/counter2.png", cl::Loader::NONE, &cache_2d_);
    lives_bg_.initialize(lives_tex, &shaders->image, true);
    lives_bg_.setTextureRect(glm::vec4(0, 35, 128, 56));

    // final image
    cl::Texture *final_tex = loader.loadTexture("data/ui/gameover.png", cl::Loader::NONE, &cache_2d_);
    cl::Texture *final_tex_good = loader.loadTexture("data/ui/finalscreen.png", cl::Loader::NONE, &cache_2d_);

    finalImage_.initialize(final_tex, &shaders->image, true);
    finalImage_.setTextureRect(glm::vec4(0, 0, 1280, 720));
    finalImage_.setPosition(glm::vec3(0.0f), cl::TOP_LEFT, true);
    
    finalImageGood_.initialize(final_tex_good, &shaders->image, true);
    finalImageGood_.setTextureRect(glm::vec4(0, 0, 1280, 720));
    finalImageGood_.setPosition(glm::vec3(0.0f), cl::TOP_LEFT, true);

    // music
    music_.push_back(loader.loadMusic("data/music/Intro.ogg", 0, NULL));
    music_.push_back(loader.loadMusic("data/music/Loop.ogg", 0, NULL));
    music_.push_back(loader.loadMusic("data/music/Ambient.ogg", 0, NULL));
    music_.push_back(loader.loadMusic("data/music/Requiem.ogg", 0, NULL));
    for (unsigned int i = 0; i < music_.size(); i++) {
        music_[i]->setLooping(false);
    }
    music_[INTRO]->setVolume(50);
    music_[LOOP]->setVolume(50);
    music_[INTRO]->play();

    // Initialize Physics
    world_.setGravity(glm::vec3(0.0f, -10.0f, 0.0f)); // Temp -- add gravity
    world_.initDebug(&shaders->bullet_debug);

    world_.addCharacter(player_.getPhysics(), COL_PLAYER, COL_ENVIRONMENT | COL_ENEMY | COL_TOWER);
    world_.addObject(&ground_, COL_ENVIRONMENT, COL_ENVIRONMENT | COL_PLAYER | COL_ENEMY | COL_TOWER);

    cl::File *collisions = loader.loadFile("data/level/level.ccf", cl::Loader::NONE, NULL);
    collisions_.initialize(collisions->contents);
    delete collisions;

    world_.addCollisions(&collisions_, COL_ENVIRONMENT, COL_ENVIRONMENT | COL_PLAYER | COL_ENEMY | COL_TOWER);

    initWaves();
    // wave data
    cl::File *file = loader.loadFile("data/waves.config", cl::Loader::NONE, NULL);
    wave_settings_.parse(file->contents);
    delete file;
    for (unsigned int i = 1; i <= 15; i++) {
        std::string cat = cl::toStringI(i);
        waves_.push_back(new Wave(
                                    wave_settings_.getIntKey(cat, "Level"),
                                    wave_settings_.getFloatKey(cat, "SpawnTime"),
                                    wave_settings_.getIntKey(cat, "Knights"),
                                    wave_settings_.getIntKey(cat, "FKnights"),
                                    wave_settings_.getIntKey(cat, "Bosses"),
                                    wave_settings_.getFloatKey(cat, "Length"),
                                    wave_settings_.getIntKey(cat, "Path"),
                                    wave_settings_.getFloatKey(cat, "Scale")
                    )); 
    }
}


void Level::update(float dt)
{
    if (finished_) {
        for (unsigned int i = 0; i < music_.size(); i++) {
            music_[i]->stop();
        }
        // display an image
        // and return
        return;
    }
    player_.update(&camera_, &enemies_, dt);

    // music
    if (music_[INTRO]->hasFinished()) {
        music_[LOOP]->play();
    }

    // Update Towers
    for (int i = 0; i < static_cast<int>(towers_.size()); i++)
    {
        towers_[i]->update(dt);
        towers_[i]->setRotation(glm::vec3(0.0f));
                                                                
        if (towers_[i]->destroy())
        {
            delete towers_[i];
            towers_.erase(towers_.begin() + i);
            i--;
        }
    }

    // Update Bullets
    for (unsigned int i = 0; i < towers_.size(); i++)
    {
        glm::vec3 dir;

        if (getClosestEnemyLocation(towers_[i], &dir) && towers_[i]->getType() != 0)
        {
            towers_[i]->shoot(towers_[i]->getPosition() + glm::vec3(0.0f, 2.5f, 0.0f), dir, &world_);
        }

        for (int j = 0; j < static_cast<int>(towers_[i]->getBullets()->size()); j++)
        {
            Bullet *bullet = towers_[i]->getBullets()->at(j);

            bullet->update(dt);

            if (bullet->destroy())
            {
                delete bullet;
                towers_[i]->getBullets()->erase(towers_[i]->getBullets()->begin() + j);
                j--;
            }
            // Perform hitting on enemy
            else
            {
                if (bullet->isAlive()) {
                    for (unsigned int k = 0; k < enemies_.size(); k++)
                    {
                        if (world_.hasIntersected(bullet->getHitbox(), enemies_[k]->getPhysics(), NULL))
                        {
                            if (bullet->getAoE() > 0.0f)
                            {
                                for (unsigned int l = 0; l < enemies_.size(); l++)
                                {
                                    float d = glm::length(enemies_[l]->getPosition() - bullet->getPosition());
                                    if (d <= bullet->getAoE())
                                    {
                                        enemies_[l]->takeDamage(towers_[i]->getDamage());
                                        enemies_[l]->setBuff(towers_[i]->getBuff());
                                    }
                                }
                            }
                            else
                            {
                                enemies_[k]->takeDamage(towers_[i]->getDamage());
                                enemies_[k]->setBuff(towers_[i]->getBuff());
                            }

                            SoundManager* sm = SoundManager::instance();
                            if (glm::distance(sm->getPlayer()->getPosition(),  towers_[i]->getPosition()) < SOUND_FALLOFF) {
                                switch (towers_[i]->getType()) {
                                    case WALL:
                                    case ICE: sm->at(ICE_HIT)->play(); break;
                                    case LIGHTNING: sm->at(LIGHTNING_HIT)->play(); break;
                                    case FIRE: sm->at(FIRE_HIT)->play(); break;
                                    case POISON: sm->at(POISON_HIT)->play(); break;
                                    //CRYSTAL: sound_[CRYSTAL_HIT]->play(); break;
                                }
                            }
                            bullet->kill();
                            break;
                        }
                    }
                } 
            }
        }
    } // end bullet update

    // if there is a wave left
    if (cur_wave_index_ < waves_.size()) {
        processWave(waves_[cur_wave_index_], dt);
    } else {
        if (enemies_.empty())
        {
            finished_ = true;
            finished_good_ = true;
        }
    }

    for (size_t i = 0; i < enemies_.size(); i++) {
        enemies_[i]->update(NULL, dt);
        if (!enemies_.empty() && !ritualists_.empty() && enemies_[i]->noMoreWaypoints()) {
            SoundManager* sm = SoundManager::instance();
            sm->at(RITUALIST)->play();
            delete ritualists_[ritualists_.size()-1];
            ritualists_.pop_back();
            Enemy *ep = enemies_[i];
            enemies_.erase(enemies_.begin() + i);
            delete ep;
        }
        if (enemies_[i]->destroy()) { 
            Crystal *c = new Crystal();
            c->initialize(&sp->model_static, &crystal_cache_);
            c->setAmmount(enemies_[i]->getReward());
            c->setPosition(enemies_[i]->getPosition() - glm::vec3(0.0f, 0.5f, 0.0f), true);
            crystals_.push_back(c);

            enemies_[i]->getPhysics()->removeFromParentWorld();
            enemies_[i]->die();
            SoundManager* sm = SoundManager::instance();
            if (glm::distance(sm->getPlayer()->getPosition(),  enemies_[i]->getPosition()) < SOUND_FALLOFF) {
                sm->at(DEATH)->play();
            }
            Enemy *ep = enemies_[i];
            enemies_.erase(enemies_.begin() + i);
            delete ep;
        }
    }
    if (ritualists_.size() == 0) {
        //std::cout << "Game over" << std::endl;
        finished_ = true;
    }
    for (size_t i = 0; i < ritualists_.size(); i++)
        ritualists_[i]->update(NULL, dt);

    // Update crystals
    for (int i = 0; i < static_cast<int>(crystals_.size()); i++)
    {
        crystals_[i]->update(&player_, dt);
        
        if (crystals_[i]->destroy())
        {
            SoundManager* sm = SoundManager::instance();
            sm->at(PICKUP)->play();
            delete crystals_[i];
            crystals_.erase(crystals_.begin() + i);
            i--;
        }
    }

    // Runs all physics -- updates position, collides, etc
    world_.simulate(dt);

    player_.processGrab();

    camera_.updateShake(dt);
    camera_.moveTo(player_.getPosition(), dt);

    // Set 2D Positions
    currency_bg_.setPosition(glm::vec3(1270.0f, 10.0f, 0.0f), cl::TOP_RIGHT, true);
    wave_bg_.setPosition(glm::vec3(1270.0f, 80.0f, 0.0f), cl::TOP_RIGHT, true);
    lives_bg_.setPosition(glm::vec3(1270.0f, 150.0f, 0.0f), cl::TOP_RIGHT, true);

    currency_text_.setString(cl::toStringUI(player_.getCrystals()));
    currency_text_.setPosition(currency_bg_.getPosition() + glm::vec3(18.0f, 3.0f, 0.0f), cl::CENTRE, true);

    if (cur_wave_index_ < waves_.size())
        wave_text_.setString(cl::toStringI(waves_[cur_wave_index_]->level));
    wave_text_.setPosition(wave_bg_.getPosition() + glm::vec3(18.0f, 3.0f, 0.0f), cl::CENTRE, true);

    lives_text_.setString(cl::toStringUI(ritualists_.size()));
    lives_text_.setPosition(lives_bg_.getPosition() + glm::vec3(18.0f, 3.0f, 0.0f), cl::CENTRE, true);
}

bool Level::getClosestEnemyLocation(Tower *tower, glm::vec3 *output)
{
    float max_dist = FLT_MAX;
    bool found = false;

    for (unsigned int i = 0; i < enemies_.size(); i++)
    {
        if (!enemies_[i]->isAlive())
            continue;

        float d = glm::length(tower->getPosition() - enemies_[i]->getPosition());

        if (d < max_dist && d < tower->getAttackRange())
        {
            glm::vec3 pos = glm::normalize(enemies_[i]->getPosition() - tower->getPosition() - glm::vec3(0.0f, 1.5f, 0.0f));

            pos += 0.1f * enemies_[i]->getPhysics()->getVelocity();

            *output = pos;
            found = true;
            max_dist = d;
        }
    }

    return found;
}

void Level::draw(cl::Renderer *renderer)
{
    if (finished_) 
    {
        renderer->setInterpolation(1.0f);
        renderer->setCamera(&camera_2d_);

        if (finished_good_)
            renderer->draw(&finalImageGood_);
        else
            renderer->draw(&finalImage_);
            
        return;
    }
    camera_.updateViewMatrix(renderer->getInterpolation());
    renderer->setCamera(&camera_);

    renderer->draw(&level_model_);
    renderer->draw(&player_);

    if (player_.isSummoning())
        renderer->draw(player_.getSummonImage());

    if (player_.getBuildMenuImage() != NULL)
        renderer->draw(player_.getBuildMenuImage());

    if (player_.getBuildMenuCostText() != NULL)
        renderer->draw(player_.getBuildMenuCostText());

    if (player_.getSelectedSampleTower() != NULL)
        renderer->draw(player_.getSelectedSampleTower());

    if (player_.getUpgradeMenuImage() != NULL)
        renderer->draw(player_.getUpgradeMenuImage());

    if (player_.getUpgradeMenuCostText() != NULL)
        renderer->draw(player_.getUpgradeMenuCostText());

    for (unsigned int i = 0; i < towers_.size(); i++)
    {
        if (towers_[i]->isAlive())
            renderer->draw(towers_[i]);

        renderer->draw(towers_[i]->getParticles());

        for (unsigned int j = 0; j < towers_[i]->getBullets()->size(); j++)
        {
            if (towers_[i]->getBullets()->at(j)->isAlive())
            {
                towers_[i]->getBullets()->at(j)->setScaling(glm::vec3(1.0f));
                renderer->draw(towers_[i]->getBullets()->at(j));
                // world_.drawDebug(towers_[i]->getBullets()->at(j)->getHitbox(), &camera_);
            }

            renderer->draw(towers_[i]->getBullets()->at(j)->getParticles());
        }
    }
    for (unsigned int i = 0; i < enemies_.size(); i++) {
        renderer->draw(enemies_[i]);
    }
    for (unsigned int i = 0; i < ritualists_.size(); i++) {
        renderer->draw(ritualists_[i]);
    }

    for (unsigned int i = 0; i < crystals_.size(); i++)
    {
        renderer->draw(crystals_[i]);
    }

    renderer->setCamera(&camera_2d_);

    renderer->draw(&currency_bg_);
    renderer->draw(&currency_text_);

    renderer->draw(&wave_bg_);
    renderer->draw(&wave_text_);

    renderer->draw(&lives_bg_);
    renderer->draw(&lives_text_);
    
    renderer->printOGLErrors();
    // Debug drawing of physics
    // world_.drawAllDebug(&camera_);
}

void Level::processWave(Wave* wave, float dt)
{
    wave->wave_timer += dt;
    wave->spawn_timer += dt;

    if (wave->spawn_timer >= wave->spawn_time && wave->enemies_spawned < wave->num_knights_enemies + wave->num_fast_knights_enemies + wave->num_boss_enemies)
    {
        Enemy *k;

        if (wave->num_knights_enemies > 0)
        {
            k = new Knight(waypoints_[wave->wp_index], wave->enemy_scale);
        }
        else if (wave->num_fast_knights_enemies > 0)
        {
            //k = new Knight(waypoints_[wave->wp_index], wave->enemy_scale);
        }
        else if (wave->num_boss_enemies > 0)
        {
            k = new BossKnight(waypoints_[wave->wp_index], wave->enemy_scale);
        }

        k->initialize(&sp->model_rigged);
        world_.addCharacter(k->getPhysics(), COL_ENEMY, COL_ENVIRONMENT | COL_PLAYER | COL_ENEMY | COL_BULLET | COL_TOWER);
        enemies_.push_back(k);

        wave->enemies_spawned++;
        wave->spawn_timer = 0;
    }
    if (wave->wave_timer >= wave->wave_time) 
    {
        cur_wave_index_++;
    }
}

void Level::initWaves(void) {
    // waypoints
    waypoints_.push_back(std::vector<glm::vec3>());
    waypoints_[0].push_back(glm::vec3(64.35, 0, 16.5));
    waypoints_[0].push_back(glm::vec3(51.15, 0, 16.5));
    waypoints_[0].push_back(glm::vec3(51.15, 0, 9.9));
    waypoints_[0].push_back(glm::vec3(47.85, 0, 9.9));
    waypoints_[0].push_back(glm::vec3(47.85, 0, 14.85));
    waypoints_[0].push_back(glm::vec3(37.95, 0, 14.85));
    // end of starter common
    waypoints_[0].push_back(glm::vec3(37.95, 0, 8.25));
    waypoints_[0].push_back(glm::vec3(31.35, 0, 8.25));
    waypoints_[0].push_back(glm::vec3(31.35, 0, 13.2));
    waypoints_[0].push_back(glm::vec3(14.85, 0, 13.2));
    waypoints_[0].push_back(glm::vec3(14.85, 0, 33.0));
    waypoints_[0].push_back(glm::vec3(8.25, 0, 33.0));
    // start of common ender 
    waypoints_[0].push_back(glm::vec3(8.25, 0, 28.05));
    waypoints_[0].push_back(glm::vec3(8.25, 0, 21.45));
    waypoints_[0].push_back(glm::vec3(-8.25, 0, 21.45));
    waypoints_[0].push_back(glm::vec3(-8.25, 0, 16.5));
    waypoints_[0].push_back(glm::vec3(0, 0, 16.5));
    waypoints_[0].push_back(glm::vec3(0, 0, 5.0));

    waypoints_.push_back(std::vector<glm::vec3>());
    waypoints_[1].push_back(glm::vec3(64.35, 0, 16.5));
    waypoints_[1].push_back(glm::vec3(51.15, 0, 16.5));
    waypoints_[1].push_back(glm::vec3(51.15, 0, 9.9));
    waypoints_[1].push_back(glm::vec3(47.85, 0, 9.9));
    waypoints_[1].push_back(glm::vec3(47.85, 0, 14.85));
    waypoints_[1].push_back(glm::vec3(37.95, 0, 14.85));
    // end of starter common
    waypoints_[1].push_back(glm::vec3(37.95, 0, 8.25));
    waypoints_[1].push_back(glm::vec3(31.35, 0, 8.25));
    waypoints_[1].push_back(glm::vec3(31.35, 0, 13.2));
    waypoints_[1].push_back(glm::vec3(14.85, 0, 13.2));
    waypoints_[1].push_back(glm::vec3(14.85, 0, 33.0));
    waypoints_[1].push_back(glm::vec3(3.3, 0, 33.0));
    waypoints_[1].push_back(glm::vec3(-4.95, 0, 33.0));
    waypoints_[1].push_back(glm::vec3(-4.95, 0, 28.05));
    // start of common ender 
    waypoints_[1].push_back(glm::vec3(8.25, 0, 28.05));
    waypoints_[1].push_back(glm::vec3(8.25, 0, 21.45));
    waypoints_[1].push_back(glm::vec3(-8.25, 0, 21.45));
    waypoints_[1].push_back(glm::vec3(-8.25, 0, 16.5));
    waypoints_[1].push_back(glm::vec3(0, 0, 16.5));
    waypoints_[1].push_back(glm::vec3(0, 0, 5.0));

    waypoints_.push_back(std::vector<glm::vec3>());
    waypoints_[2].push_back(glm::vec3(64.35, 0, 16.5));
    waypoints_[2].push_back(glm::vec3(51.15, 0, 16.5));
    waypoints_[2].push_back(glm::vec3(51.15, 0, 9.9));
    waypoints_[2].push_back(glm::vec3(47.85, 0, 9.9));
    waypoints_[2].push_back(glm::vec3(47.85, 0, 14.85));
    waypoints_[2].push_back(glm::vec3(37.95, 0, 14.85));
    // end of starter common
    waypoints_[2].push_back(glm::vec3(37.95, 0, 26.4));
    waypoints_[2].push_back(glm::vec3(31.35, 0, 26.4));
    waypoints_[2].push_back(glm::vec3(31.35, 0, 21.45));
    waypoints_[2].push_back(glm::vec3(21.45, 0, 21.45));
    waypoints_[2].push_back(glm::vec3(21.45, 0, 24.75));
    waypoints_[2].push_back(glm::vec3(19.8, 0, 24.75));
    waypoints_[2].push_back(glm::vec3(19.8, 0, 33.0));
    waypoints_[2].push_back(glm::vec3(8.24, 0, 33.0));
    // start of common ender 
    waypoints_[2].push_back(glm::vec3(8.25, 0, 28.05));
    waypoints_[2].push_back(glm::vec3(8.25, 0, 21.45));
    waypoints_[2].push_back(glm::vec3(-8.25, 0, 21.45));
    waypoints_[2].push_back(glm::vec3(-8.25, 0, 16.5));
    waypoints_[2].push_back(glm::vec3(0, 0, 16.5));
    waypoints_[2].push_back(glm::vec3(0, 0, 5.0));

    waypoints_.push_back(std::vector<glm::vec3>());
    waypoints_[3].push_back(glm::vec3(64.35, 0, 16.5));
    waypoints_[3].push_back(glm::vec3(51.15, 0, 16.5));
    waypoints_[3].push_back(glm::vec3(51.15, 0, 9.9));
    waypoints_[3].push_back(glm::vec3(47.85, 0, 9.9));
    waypoints_[3].push_back(glm::vec3(47.85, 0, 14.85));
    waypoints_[3].push_back(glm::vec3(37.95, 0, 14.85));
    // end of starter common
    waypoints_[3].push_back(glm::vec3(37.95, 0, 26.4));
    waypoints_[3].push_back(glm::vec3(31.35, 0, 26.4));
    waypoints_[3].push_back(glm::vec3(31.35, 0, 21.45));
    waypoints_[3].push_back(glm::vec3(21.45, 0, 21.45));
    waypoints_[3].push_back(glm::vec3(21.45, 0, 24.75));
    waypoints_[3].push_back(glm::vec3(19.8, 0, 24.75));
    waypoints_[3].push_back(glm::vec3(19.8, 0, 33.0));
    waypoints_[3].push_back(glm::vec3(-4.95, 0, 33.0));
    waypoints_[3].push_back(glm::vec3(-4.95, 0, 28.05));
    // start of common ender 
    waypoints_[3].push_back(glm::vec3(8.25, 0, 28.05));
    waypoints_[3].push_back(glm::vec3(8.25, 0, 21.45));
    waypoints_[3].push_back(glm::vec3(-8.25, 0, 21.45));
    waypoints_[3].push_back(glm::vec3(-8.25, 0, 16.5));
    waypoints_[3].push_back(glm::vec3(0, 0, 16.5));
    waypoints_[3].push_back(glm::vec3(0, 0, 5.0));
}
