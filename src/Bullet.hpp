#pragma once
#include <Clone/3D/Entity3D.hpp>
#include <Clone/ParticleEmitter.hpp>
#include <Clone/Bullet/BulletPhysicsWorld.hpp>
#include <Clone/AssetLoader.hpp>
#include <Clone/GameSettings.hpp>
#include "ParticleEffects.hpp"

class Bullet : public cl::Entity3D
{
 public:
    Bullet();

    void initialize(cl::GameSettings *gs, const std::string &bullet_cat, const glm::vec3 &position, const glm::vec3 &dir, cl::Shader *shader, cl::Shader *particle_shader, int type);
    void update(float dt);

    void kill();
    bool isAlive() const;
    bool destroy() const;

    void setAoE(float aoe);
    float getAoE() const;

    cl::BulletPhysicsObject* getPhysics();
    cl::BulletTriggerObject* getHitbox();
    cl::ParticleEmitter* getParticles();

 private:
    static cl::AssetCache bullet_cache_;
    btSphereShape shape_;
    btSphereShape hitbox_shape_;
    bool alive_;
    float life_time_;
    float life_tick_;
    float aoe_;
    glm::vec3 direction_;
    cl::BulletTriggerObject hitbox_;
    cl::BulletPhysicsObject physics_;
    cl::ModelData *model_data_;
    cl::ParticleEmitter particles_flying_;
    cl::ParticleEmitter particles_impact_;
};
