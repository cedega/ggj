#pragma once
#include <Clone/3D/Entity3D.hpp>
#include <Clone/AssetLoader.hpp>
#include "Player.hpp"
#include "CrystalPhysics.hpp"

class Crystal : public cl::Entity3D
{
 public:
    Crystal();

    void initialize(cl::Shader *shader, cl::AssetCache *cache);
    void update(Player *player, float dt);

    void setAmmount(float ammount);
    float getAmmount() const;

    bool destroy() const;

 private:
    bool alive_;
    bool track_;
    float ammount_;
    float speed_;
    CrystalPhysics physics_;
};
