#pragma once
#include <vector>
#include <Clone/3D/Entity3D.hpp>
#include <Clone/3D/Armature.hpp>
#include <Clone/Bullet/BulletCharacterObject.hpp>
#include <Clone/Bullet/BulletPhysicsWorld.hpp>
#include <Clone/AssetLoader.hpp>
#include <Clone/Input.hpp>
#include <Clone/Image.hpp>
#include "Tower.hpp"
#include "UpgradeMenu.hpp"
#include "CollisionMask.hpp"

class Enemy;
class Tower;

struct PlayerInput
{
    glm::vec2 move_dir;
    bool jump;
    bool build_show;
    bool build_next;
    bool build_prev;
    bool build_create;
    bool grab;
    bool camera_left;
    bool camera_right;
    bool upgrade_show;
    bool upgrade;

    PlayerInput() :
        move_dir(0.0f),
        jump(false),
        build_show(false),
        build_next(false),
        build_prev(false),
        build_create(false),
        grab(false),
        camera_left(false),
        camera_right(false),
        upgrade_show(false),
        upgrade(false)
    {

    }
};

class Player : public cl::Entity3D
{
 public:
    Player();
    ~Player();

    void uploadToShader(cl::Shader *shader, float interpolation, int draw_index);

    void initialize(std::vector<Tower*> *towers, std::vector<Enemy*> *enemies, cl::BulletPhysicsWorld *world, cl::Shader *tower_shader, cl::Shader *image_shader, cl::Shader *player_shader, cl::Shader *billboard_shader, cl::Shader *particle_shader, cl::Shader *bullet);
    void update(cl::Camera *camera, std::vector<Enemy*> *enemies, float dt);
    void processGrab();
    void addCrystals(float crystals);
    float getCrystals() const;

    bool isSummoning() const;

    cl::BulletCharacterObject* getPhysics();
    cl::Image* getSummonImage();
    cl::Image* getBuildMenuImage();
    cl::Text* getBuildMenuCostText();
    cl::Text* getUpgradeMenuCostText();
    cl::Image* getUpgradeMenuImage();
    Tower* getSelectedSampleTower();

 private:
    btCapsuleShape physics_shape_;
    cl::ModelData *model_data_; 
    cl::Armature armature_;
    cl::BulletCharacterObject physics_;
    cl::SkeletalAnimation anim_idle_;
    cl::SkeletalAnimation anim_lift_;
    cl::SkeletalAnimation anim_drop_;
    cl::SkeletalAnimation anim_push_;
    cl::SkeletalAnimation anim_run_;
    cl::SkeletalAnimation anim_run_holding_;
    cl::SkeletalAnimation anim_jump_;
    cl::SkeletalAnimation anim_fall_;

    cl::GameSettings tower_settings_;
    std::vector<Tower*> *towers_;
    std::vector<Enemy*> *enemies_;
    cl::BulletPhysicsWorld *world_;
    cl::Shader *tower_shader_;
    cl::Shader *bullet_shader_;
    cl::Shader *particle_shader_;
    cl::AssetCache tower_cache_;

    PlayerInput input_;
    PlayerInput last_input_;
    BuildMenu build_menu_;
    UpgradeMenu upgrade_menu_;
    glm::vec3 upgrade_location_;

    glm::vec3 summon_location_;
    std::string summon_tower_category_;
    float summoning_tick_;
    float summoning_time_;
    int tower_bone_index_;
    bool can_place_tower_;
    bool has_created_a_tower_;
    bool done_summon_; // used to stop infinite sound effect 
    bool play_push_;
    float crystals_;

    glm::vec3 camera_direction_;
    float camera_rotation_;

    void getInputAsKeyboard();
    void getInputAsController();

    Tower *grabbing_tower_;

    Tower sample_towers_[NUM_TOWERS];

    cl::Texture *summon_texture_;
    cl::Image summon_image_;
    
    cl::Sound *summon_sound_;
    
    void processSummon(float dt);
    void input(cl::Camera *camera, float dt);
    void processInput(float dt);
    void animations(float dt);
    bool createTower(TOWERS type);
    glm::vec3 getCreateTowerPosition() const;
};

