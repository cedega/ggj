#include "CrystalPhysics.hpp"

CrystalPhysics::CrystalPhysics()
{

}

void CrystalPhysics::update(float dt)
{
    position_ += velocity_ * dt;
}

void CrystalPhysics::setPosition(const glm::vec3 &position)
{
    position_ = position;
}

glm::vec3 CrystalPhysics::getPosition() const
{
    return position_;
}

void CrystalPhysics::setRotation(const glm::vec3 &rotation)
{
    rotation_ = rotation;
}

glm::vec3 CrystalPhysics::getRotation() const
{
    return rotation_;
}

void CrystalPhysics::setVelocity(const glm::vec3 &velocity)
{
    velocity_ = velocity;
}

glm::vec3 CrystalPhysics::getVelocity() const
{
    return velocity_;
}

