#include "Crystal.hpp"

Crystal::Crystal() :
    alive_(true),
    track_(false),
    ammount_(0),
    speed_(2.0f)
{

}

void Crystal::initialize(cl::Shader *shader, cl::AssetCache *cache)
{
    cl::AssetLoader loader;
    cl::ModelData *model_data = loader.loadModelData("data/crystal/dark-crystal.cm", cl::Loader::NONE, cache);

    cl::Entity3D::initialize(model_data, shader, &physics_); 
    cl::Entity3D::setModelOffset(glm::vec3(0.0f, 0.0f, 0.0f));
    cl::Entity3D::setCullAABB(cl::AABB(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.6f, 0.6f, 0.6f)));
    cl::Entity3D::setScaling(glm::vec3(0.25f));
}

void Crystal::update(Player *player, float dt)
{
    if (destroy())
        return;

    float distance = glm::length(player->getPosition() - getPosition());

    if (distance < 2.5f)
    {
        track_ = true;
    }

    if (track_)
    {
        physics_.setVelocity(speed_ * glm::normalize(player->getPosition() - getPosition()));
        physics_.update(dt);
        speed_ += 10.0f * dt;

        if (glm::length(player->getPosition() - getPosition()) < 0.3f)
        {
            player->addCrystals(ammount_);
            alive_ = false;
        }
    }

    cl::Entity3D::setRotation(cl::Entity3D::getRotation() + glm::vec3(0.0f, 3.0f * dt, 0.0f));
    cl::Entity3D::update(dt);
}

void Crystal::setAmmount(float ammount)
{
    ammount_ = ammount;
}

float Crystal::getAmmount() const
{
    return ammount_;
}

bool Crystal::destroy() const
{
    return !alive_;
}

