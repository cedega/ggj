#pragma once
#include "Enemy.hpp"

class Knight : public Enemy
{ // i put this brace on a separate line just for you isaac

 public:
    Knight(float multiplier);
    Knight(std::vector<glm::vec3> waypoints, float multiplier);
    void initialize(cl::Shader *shader);
    cl::BulletCharacterObject* getPhysics();
    void update(cl::Camera *camera, float dt);
    void takeDamage(float damage);

 private:
    cl::ModelData *model_data_;
    cl::SkeletalAnimation anim_run_;
    cl::SkeletalAnimation anim_die_;
    btCapsuleShape physics_shape_;
    cl::BulletCharacterObject physics_; 
};

