#include "ParticleEffects.hpp"

// ------------------------------
//   ICE PARTICLES
// ------------------------------

void ice_flying(cl::Particle *particle, int index, void *data)
{
    glm::vec3 direction = *static_cast<glm::vec3*>(data);

    const float tick = 360.0f / 500.0f;
    const float u = index * tick;
    const float v = cl::randomF(0.0f, 180.0f);

    particle->life_time = cl::randomF(0.2f, 1.5f);

    particle->position.x = cl::randomF(0.1f, 0.2f)*std::cos(glm::radians(u))*std::sin(glm::radians(v)); 
    particle->position.y = cl::randomF(0.1f, 0.2f)*std::cos(glm::radians(v));
    particle->position.z = cl::randomF(0.1f, 0.2f)*std::sin(glm::radians(u))*std::sin(glm::radians(v));

    float base = cl::randomF(0.0f, 0.4f);
    particle->colour = glm::vec4(base, base, 1.0f, 0.5f);

    particle->velocity = -4.0f * direction;
    particle->acceleration = -0.2f * particle->velocity;
}

void ice_impact(cl::Particle *particle, int index, void *data)
{
    const float tick = 360.0f / 500.0f;
    const float u = index * tick;
    const float v = cl::randomF(0.0f, 180.0f);

    particle->life_time = cl::randomF(0.3f, 0.5f);

    particle->position.x = cl::randomF(0.1f, 0.5f)*std::cos(glm::radians(u))*std::sin(glm::radians(v)); 
    particle->position.y = cl::randomF(0.1f, 0.5f)*std::cos(glm::radians(v));
    particle->position.z = cl::randomF(0.1f, 0.5f)*std::sin(glm::radians(u))*std::sin(glm::radians(v));

    float base = cl::randomF(0.0f, 1.0f);

    particle->colour = glm::vec4(base, base, 1.0f, cl::randomF(0.4f, 1.0f));

    particle->velocity = 8.0f * particle->position;
    particle->acceleration = -0.9f * particle->velocity;
}

// ------------------------------
//   FIRE PARTICLES
// ------------------------------

void fire_flying(cl::Particle *particle, int index, void *data)
{
    glm::vec3 direction = *static_cast<glm::vec3*>(data);

    const float tick = 360.0f / 500.0f;
    const float u = index * tick;
    const float v = cl::randomF(0.0f, 180.0f);

    particle->life_time = cl::randomF(0.2f, 0.8f);

    particle->position.x = cl::randomF(0.2f, 0.3f)*std::cos(glm::radians(u))*std::sin(glm::radians(v)); 
    particle->position.y = cl::randomF(0.2f, 0.3f)*std::cos(glm::radians(v));
    particle->position.z = cl::randomF(0.2f, 0.3f)*std::sin(glm::radians(u))*std::sin(glm::radians(v));

    float base = cl::randomF(0.0f, 0.4f);
    particle->colour = glm::vec4(1.0f, base + 0.3f, base, 0.8f);

    particle->velocity = -4.0f * direction;
    particle->acceleration = 0.0f * particle->velocity;
}

void fire_impact(cl::Particle *particle, int index, void *data)
{
    const float tick = 360.0f / 500.0f;
    const float u = index * tick;
    const float v = cl::randomF(0.0f, 180.0f);

    particle->life_time = cl::randomF(0.3f, 0.5f);

    particle->position.x = cl::randomF(0.2f, 0.5f)*std::cos(glm::radians(u))*std::sin(glm::radians(v)); 
    particle->position.y = cl::randomF(0.2f, 0.5f)*std::cos(glm::radians(v));
    particle->position.z = cl::randomF(0.2f, 0.5f)*std::sin(glm::radians(u))*std::sin(glm::radians(v));

    float base = cl::randomF(0.0f, 1.0f);

    particle->colour = glm::vec4(1.0f, base, 0.0f, cl::randomF(0.6f, 1.0f));

    particle->velocity = 8.0f * particle->position;
    particle->acceleration = -0.9f * particle->velocity;
}

// ------------------------------
//   LIGHTNING PARTICLES
// ------------------------------

void elec_flying(cl::Particle *particle, int index, void *data)
{
    glm::vec3 direction = *static_cast<glm::vec3*>(data);

    const float tick = 360.0f / 500.0f;
    const float u = index * tick;
    const float v = cl::randomF(0.0f, 180.0f);

    particle->life_time = cl::randomF(0.2f, 0.8f);

    particle->position.x = cl::randomF(0.2f, 0.3f)*std::cos(glm::radians(u))*std::sin(glm::radians(v)); 
    particle->position.y = cl::randomF(0.2f, 0.3f)*std::cos(glm::radians(v));
    particle->position.z = cl::randomF(0.2f, 0.3f)*std::sin(glm::radians(u))*std::sin(glm::radians(v));

    float base = cl::randomF(0.0f, 0.3f);
    particle->colour = glm::vec4(1.0f, 1.0f, base, 0.8f);

    particle->velocity = -4.0f * direction;
    particle->acceleration = 0.0f * particle->velocity;
}

void elec_impact(cl::Particle *particle, int index, void *data)
{
    const float tick = 360.0f / 500.0f;
    const float u = index * tick;
    const float v = cl::randomF(0.0f, 180.0f);

    particle->life_time = cl::randomF(0.1f, 0.2f);

    particle->position.x = cl::randomF(0.2f, 0.5f)*std::cos(glm::radians(u))*std::sin(glm::radians(v)); 
    particle->position.y = cl::randomF(0.2f, 0.5f)*std::cos(glm::radians(v));
    particle->position.z = cl::randomF(0.2f, 0.5f)*std::sin(glm::radians(u))*std::sin(glm::radians(v));

    float base = cl::randomF(0.0f, 1.0f);

    particle->colour = glm::vec4(1.0f, 1.0f, base, cl::randomF(0.6f, 1.0f));

    particle->velocity = 16.0f * particle->position;
    particle->acceleration = -0.9f * particle->velocity;
}

// ------------------------------
//   DARK PARTICLES
// ------------------------------

void dark_flying(cl::Particle *particle, int index, void *data)
{
    glm::vec3 direction = *static_cast<glm::vec3*>(data);

    const float tick = 360.0f / 500.0f;
    const float u = index * tick;
    const float v = cl::randomF(0.0f, 180.0f);

    particle->life_time = cl::randomF(0.2f, 0.8f);

    particle->position.x = cl::randomF(0.0f, 0.4f)*std::cos(glm::radians(u))*std::sin(glm::radians(v)); 
    particle->position.y = cl::randomF(0.0f, 0.4f)*std::cos(glm::radians(v));
    particle->position.z = cl::randomF(0.0f, 0.4f)*std::sin(glm::radians(u))*std::sin(glm::radians(v));

    float base = cl::randomF(0.0f, 0.2f);
    particle->colour = glm::vec4(base, base, base, 0.6f);

    particle->velocity = -4.0f * direction;
    particle->acceleration = 0.0f * particle->velocity;
}

void dark_impact(cl::Particle *particle, int index, void *data)
{
    const float tick = 360.0f / 500.0f;
    const float u = index * tick;
    const float v = cl::randomF(0.0f, 180.0f);

    particle->life_time = cl::randomF(0.3f, 1.3f);

    particle->position.x = cl::randomF(0.2f, 0.5f)*std::cos(glm::radians(u))*std::sin(glm::radians(v)); 
    particle->position.y = cl::randomF(0.2f, 0.5f)*std::cos(glm::radians(v));
    particle->position.z = cl::randomF(0.2f, 0.5f)*std::sin(glm::radians(u))*std::sin(glm::radians(v));

    particle->colour = glm::vec4(0.0f, 0.0f, 0.0f, cl::randomF(0.6f, 1.0f));

    particle->velocity = 8.0f * particle->position;
    particle->acceleration = -0.9f * particle->velocity;
    particle->acceleration.y *= 2.0f;
}

// ------------------------------
//   POISON PARTICLES
// ------------------------------

void poison_flying(cl::Particle *particle, int index, void *data)
{
    glm::vec3 direction = *static_cast<glm::vec3*>(data);

    const float tick = 360.0f / 500.0f;
    const float u = index * tick;
    const float v = cl::randomF(0.0f, 180.0f);

    particle->life_time = cl::randomF(0.2f, 0.8f);

    particle->position.x = cl::randomF(0.0f, 0.4f)*std::cos(glm::radians(u))*std::sin(glm::radians(v)); 
    particle->position.y = cl::randomF(0.0f, 0.4f)*std::cos(glm::radians(v));
    particle->position.z = cl::randomF(0.0f, 0.4f)*std::sin(glm::radians(u))*std::sin(glm::radians(v));

    float base = cl::randomF(0.0f, 0.3f);
    particle->colour = glm::vec4(base, 1.0f, base, 0.6f);

    particle->velocity = -4.0f * direction;
    particle->acceleration = 0.0f * particle->velocity;
}

void poison_impact(cl::Particle *particle, int index, void *data)
{
    const float tick = 360.0f / 500.0f;
    const float u = index * tick;
    const float v = cl::randomF(0.0f, 180.0f);

    particle->life_time = cl::randomF(0.3f, 1.3f);

    particle->position.x = cl::randomF(0.2f, 0.5f)*std::cos(glm::radians(u))*std::sin(glm::radians(v)); 
    particle->position.y = cl::randomF(0.2f, 0.5f)*std::cos(glm::radians(v));
    particle->position.z = cl::randomF(0.2f, 0.5f)*std::sin(glm::radians(u))*std::sin(glm::radians(v));

    float base = cl::randomF(0.0, 0.5f);
    particle->colour = glm::vec4(base, 1.0f, base, cl::randomF(0.6f, 1.0f));

    particle->velocity = 4.0f * particle->position;
    particle->acceleration = -0.9f * particle->velocity;
    particle->acceleration.y *= 3.0f;
}


