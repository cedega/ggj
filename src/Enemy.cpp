#include "Enemy.hpp"

cl::AssetCache Enemy::cache_;

Enemy::Enemy(std::vector<glm::vec3> waypoints) :
    cur_wp_index_(0),
    waypoints_(waypoints)
{

}

Enemy::Enemy() :
    cur_wp_index_(0)
{

}

void Enemy::uploadToShader(cl::Shader *shader, float interpolation, int draw_index)
{
    cl::Entity3D::uploadToShader(shader, interpolation, draw_index);

    shader->setUniformM44v("bones[0]", &armature_.getMatrixTransforms()[0], armature_.getMatrixTransforms().size());
}

void Enemy::initialize(cl::Shader *shader)
{
    // cl::SkeletalAnimation *anim_idle = loader.loadSkeletalAnimation("", cl::Loader::NONE, NULL, false);
    // anim_idle_ = *anim_idle;
    // delete anim_idle; 

    buff_.slow = buff_.dot =  false;
    buff_.length = buff_.tickdmg = 0;
    cl::Entity3D::setCullAABB(cl::AABB(glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(1.5f, 2.0f, 1.5f)));
    cl::Entity3D::setPosition(glm::vec3(66.3, 0, 16.5), false);

}

void Enemy::update(cl::Camera *camera, float dt)
{
    input(camera, dt); 
    // animations(dt);

    // cl::Entity3D::update(dt);
}

void Enemy::input(cl::Camera *camera, float dt)
{
    ai();
    if (buff_.dot == true) {
        takeDamage(buff_.tickdmg * dt);
    }
    if (buff_.slow == true) {
        cl::Entity3D::setMoveSpeed(movespeed_ * 0.50f);
    }
    else {
        cl::Entity3D::setMoveSpeed(movespeed_);
    }
    updateBuff(dt);
    glm::vec2 dir = faceWaypoint();
    cl::Entity3D::input(dir, camera, false, dt);
}

void Enemy::animations(float dt)
{
    // None for now

    armature_.update(dt);
}


// runs all relevant ai routines
void Enemy::ai(void) {
    // check if an enemy is at the end
    if (cur_wp_index_ >= waypoints_.size()) {
        return;
    }
    if (hasReachedWaypoint()) {
        if (nextWaypoint() < 0) {
            // no waypoints left
            // should not move because faceWaypoint will set movespeed to 0
            return;
        } 
        // anything additional upon reaching a waypoint
    }
    else { 
        // anything that should happen during a normal walk
        // note that an enemy that has no waypoints left will always hit this point
    }
}

// gets the direction for the next waypoint
glm::vec2 Enemy::faceWaypoint() {
    glm::vec2 dir;
    // if valid index
    if (cur_wp_index_ < waypoints_.size()) {
        glm::vec3 diff = waypoints_[cur_wp_index_] - getPosition();
        dir = glm::vec2(diff.x, diff.z);
    } 
    // invalid index, set move speed to 0
    else {
        dir = glm::vec2(0,0);
        Entity3D::setMoveSpeed(0.0f);
    } 
    return dir;
}

// set the waypoints of an enemy
// redundant considering the constructor
// included mostly in case an enemy would need to change
// waypoints halfway through its life (????)
void Enemy::setWaypoint(std::vector<glm::vec3> waypoints) {
    cur_wp_index_ = 0;
    waypoints_ = waypoints;
}

// returns true if an enemy has reached its destination
bool Enemy::hasReachedWaypoint() {
    float delta = 0.25f;
    float diff_x;
    float diff_z;
    // check if final destination reached
    if (cur_wp_index_ >= waypoints_.size()) {
        return false;
    }
    diff_x = glm::abs(getPosition().x - waypoints_[cur_wp_index_].x);
    diff_z = glm::abs(getPosition().z - waypoints_[cur_wp_index_].z);
    return (diff_x <= delta && diff_z <= delta);
}

// increments the enemy to the next waypoint
// returns the index of the waypoint
// or -1 if the last waypoint has been reached
int Enemy::nextWaypoint() { 
    if (++cur_wp_index_ >= waypoints_.size()) {
        return -1;
    }
    return cur_wp_index_;
}

void Enemy::die() {
    // trigger fading animation
    alive_ = false;
    //physics_->removeFromParentWorld();
}

bool Enemy::isAlive() const {
    return health_ > 0.0f;
}

bool Enemy::destroy() {
    return !isAlive() && !alive_ && armature_.getLoadedAnimation()->finished();
}

float Enemy::getReward() const {
    return reward_;
}

bool Enemy::noMoreWaypoints() const {
    return (cur_wp_index_ >= waypoints_.size());
}

void Enemy::takeDamage(float damage) {
    health_ -= damage;
}

Buff Enemy::getBuff() const {
    return buff_;
}

void Enemy::setBuff(Buff buff) {
    buff_.slow |= buff.slow;
    buff_.dot |= buff.dot;
    // dots stack duration
    buff_.length += buff.length;
    // don't change tickdmg, highest sticks
    buff_.tickdmg = buff_.tickdmg >= buff.tickdmg ? buff_.tickdmg : buff.tickdmg;
}

// call AFTER setBuff in level
void Enemy::updateBuff(float dt) {
    if (buff_.slow || buff_.dot) {
        buff_.tick += dt;
    }
    if (buff_.tick >= buff_.length) {
        buff_.slow = buff_.dot = false;
        buff_.tickdmg = 0;
        buff_.length = 0;
    }
}
