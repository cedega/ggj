#include "Player.hpp"
#include "Enemy.hpp"
#include "Tower.hpp"
#include <iostream>

Player::Player() :
    physics_shape_(0.4f * 0.5f, 1.65f * 0.5f),
    physics_(&physics_shape_, 1.65f * 0.5f, 10.0f, glm::quat(glm::vec3(0.0f)), glm::vec3(0.0f, 0.825f, 0.0f)),
    summoning_tick_(0.0f),
    summoning_time_(0.0f),
    can_place_tower_(false),
    has_created_a_tower_(false),
    done_summon_(false),
    play_push_(false),
    crystals_(100),
    camera_rotation_(0.0f),
    grabbing_tower_(NULL)

{
    //summon_sound_ = SoundManager::instance();
}

Player::~Player()
{
    delete model_data_;
}

void Player::uploadToShader(cl::Shader *shader, float interpolation, int draw_index)
{
    cl::Entity3D::uploadToShader(shader, interpolation, draw_index);

    shader->setUniformM44v("bones[0]", &armature_.getMatrixTransforms()[0], armature_.getMatrixTransforms().size());
}

void Player::initialize(std::vector<Tower*> *towers, std::vector<Enemy*> *enemies, cl::BulletPhysicsWorld *world, cl::Shader *tower_shader, cl::Shader *image_shader, cl::Shader *player_shader, cl::Shader *billboard_shader, cl::Shader *particle_shader, cl::Shader *bullet)
{
    towers_ = towers;
    enemies_ = enemies;
    world_ = world;
    tower_shader_ = tower_shader;
    bullet_shader_ = bullet;
    particle_shader_ = particle_shader;

    cl::AssetLoader loader;
    model_data_ = loader.loadModelData("data/aria/aria.cm", cl::Loader::NO_NORMALS, NULL);

    cl::SkeletalAnimation *anim_idle = loader.loadSkeletalAnimation("data/aria/aria-idle.ca", cl::Loader::NONE, NULL, true);
    anim_idle_ = *anim_idle;

    cl::SkeletalAnimation *anim_lift = loader.loadSkeletalAnimation("data/aria/aria-lift.ca", cl::Loader::NONE, NULL, false);
    anim_lift_ = *anim_lift;

    cl::SkeletalAnimation *anim_drop = loader.loadSkeletalAnimation("data/aria/aria-drop.ca", cl::Loader::NONE, NULL, false);
    anim_drop_ = *anim_drop;

    cl::SkeletalAnimation *anim_push = loader.loadSkeletalAnimation("data/aria/aria-push.ca", cl::Loader::NONE, NULL, true);
    anim_push_ = *anim_push;

    cl::SkeletalAnimation *anim_run = loader.loadSkeletalAnimation("data/aria/aria-run.ca", cl::Loader::NONE, NULL, true);
    anim_run_ = *anim_run;

    cl::SkeletalAnimation *anim_run_holding = loader.loadSkeletalAnimation("data/aria/aria-run-holding.ca", cl::Loader::NONE, NULL, true);
    anim_run_holding_ = *anim_run_holding;

    cl::SkeletalAnimation *anim_jump = loader.loadSkeletalAnimation("data/aria/aria-jump.ca", cl::Loader::NONE, NULL, false);
    anim_jump_ = *anim_jump;

    cl::SkeletalAnimation *anim_fall = loader.loadSkeletalAnimation("data/aria/aria-land.ca", cl::Loader::NONE, NULL, false);
    anim_fall_ = *anim_fall;

    delete anim_idle;
    delete anim_lift;
    delete anim_drop;
    delete anim_push;
    delete anim_run;
    delete anim_run_holding;
    delete anim_jump;
    delete anim_fall;

    armature_ = *model_data_->getArmature();
    armature_.loadAnimation(&anim_idle_, 0.0f);

    summon_texture_ = loader.loadTexture("data/summon.png", cl::Loader::NONE, NULL);
    summon_image_.initialize(summon_texture_, image_shader, false);
    summon_image_.setRotation(glm::vec3(-M_PI / 2.0f, 0.0f, 0.0f));
    summon_image_.setScaling(glm::vec3(0.005f));

    cl::Entity3D::initialize(model_data_, player_shader, &physics_);

    cl::Entity3D::setPhysicsRotation(false);
    cl::Entity3D::setTurnSpeed(8.0f);
    cl::Entity3D::setMoveSpeed(5.0f);
    cl::Entity3D::setJumpSpeed(6.0f);
    cl::Entity3D::setModelOffset(glm::vec3(0.0f, -1.65f * 0.5f, 0.0f));

    cl::Entity3D::setCullAABB(cl::AABB(glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(1.5f, 2.0f, 1.5f)));

    physics_.setTriggerPointer(this);
    physics_.setTriggerIndex(0);

    tower_bone_index_ = armature_.getBoneIndexFromName("tower");

    // Tower Data
    cl::File *file = loader.loadFile("data/towers.config", cl::Loader::NONE, NULL);
    tower_settings_.parse(file->contents);
    delete file;

    build_menu_.initialize(billboard_shader);
    upgrade_menu_.initialize(billboard_shader);

    // Sample Towers
    sample_towers_[WALL].initialize(&tower_settings_, "TowerShield", tower_shader_, &tower_cache_, particle_shader_, WALL, bullet);
    sample_towers_[ICE].initialize(&tower_settings_, "TowerIce", tower_shader_, &tower_cache_, particle_shader_, ICE, bullet);
    sample_towers_[POISON].initialize(&tower_settings_, "TowerPoison", tower_shader_, &tower_cache_, particle_shader_, POISON, bullet);
    sample_towers_[FIRE].initialize(&tower_settings_, "TowerFire", tower_shader_, &tower_cache_, particle_shader_, FIRE, bullet);
    sample_towers_[CRYSTAL].initialize(&tower_settings_, "TowerCrystal", tower_shader_, &tower_cache_, particle_shader_, CRYSTAL, bullet);
    sample_towers_[LIGHTNING].initialize(&tower_settings_, "TowerLightning", tower_shader_, &tower_cache_, particle_shader_, LIGHTNING, bullet);
}

void Player::update(cl::Camera *camera, std::vector<Enemy*> *enemies, float dt)
{
    processSummon(dt);
    
    if (isSummoning())
        cl::Entity3D::setMoveSpeed(0.0f);
    else
        cl::Entity3D::setMoveSpeed(5.0f);

    input(camera, dt); 
    animations(dt);

    cl::Entity3D::update(dt);

    // Kill any towers if it's touching an enemy while spawning

    if (isSummoning())
    {
        Tower *summon = towers_->back();

        for (unsigned int j = 0; j < enemies_->size(); j++)
        {
            if (world_->hasIntersected(summon->getPhysics(), enemies_->at(j)->getPhysics(), NULL))
            {
                world_->removeObject(summon->getPhysics());
                summoning_tick_ = 0.0f;
                summoning_time_ = 0.0f;
                summon->kill();
                addCrystals(summon->getCost());
                SoundManager* sm = SoundManager::instance();
                summon_sound_->stop();
                sm->at(CRUMBLE)->play();
                camera->shake(0.2f, 0.2f);
                break;
            }
        }
    }

    // Update the sample towers
    
    can_place_tower_ = true;

    if (build_menu_.isSelecting())
    {
        if (getCrystals() < sample_towers_[build_menu_.getSelection()].getCost())
        {
            can_place_tower_ = false;
            sample_towers_[0].setColour(glm::vec4(1.0f, 0.2f, 0.2f, 0.5f));
        }
    }

    sample_towers_[0].updateLastPosition();
    sample_towers_[0].setPosition(getCreateTowerPosition(), false);

    for (unsigned int i = 0; i < towers_->size(); i++)
    {
        if (world_->hasIntersected(sample_towers_[0].getPhysics(), towers_->at(i)->getPhysics(), NULL))
        {
            can_place_tower_ = false;
            sample_towers_[0].setColour(glm::vec4(1.0f, 0.2f, 0.2f, 0.5f));
            break;
        }
    }

    // Player is too high up -- revoke allowance of placement
    if (can_place_tower_ && getPosition().y > 0.9f)
    {
        can_place_tower_ = false;
        sample_towers_[0].setColour(glm::vec4(1.0f, 0.2f, 0.2f, 0.5f));
    }

    if (can_place_tower_)
        sample_towers_[0].setColour(glm::vec4(0.2f, 1.0f, 0.2f, 0.5f));

    for (int i = 1; i < NUM_TOWERS; i++)
    {
        sample_towers_[i].updateLastPosition();
        sample_towers_[i].setPosition(getCreateTowerPosition(), false);

        if (can_place_tower_)
            sample_towers_[i].setColour(glm::vec4(0.2f, 1.0f, 0.2f, 0.5f));
        else
            sample_towers_[i].setColour(glm::vec4(1.0f, 0.2f, 0.2f, 0.5f));
    }

    // Remove upgrade menu if present and moved too far away

    if (upgrade_menu_.isSelecting())
    {
        if (glm::length(getPosition() - upgrade_location_) > 1.0f)
            upgrade_menu_.cancel();
    }

    float cost = 0.0f;

    if (build_menu_.isSelecting())
    {
        cost = sample_towers_[build_menu_.getSelection()].getCost();
    }

    glm::vec3 camera_cross = glm::normalize(glm::cross(camera_direction_, glm::vec3(0.0f, 1.0f, 0.0f)));
    build_menu_.update(camera, cost, getPosition() + 1.4f * camera_cross + glm::vec3(0.0f, 1.5f, 0.0f), dt);
    upgrade_menu_.update(camera, getPosition() + 1.4f * camera_cross + glm::vec3(0.0f, 1.5f, 0.0f), dt);

    if (grabbing_tower_ != NULL)
        grabbing_tower_->getPhysics()->setTransformMatrix(armature_.getBoneMatrix(tower_bone_index_, cl::Entity3D::getModelMatrix()));
}

void Player::processGrab()
{
    if (grabbing_tower_ != NULL)
    {
        bool can_drop_tower = true;

        // Re-use the sample towers to determine if we can drop
        for (unsigned int i = 0; i < towers_->size(); i++)
        {
            if (towers_->at(i) != grabbing_tower_ && world_->hasIntersected(sample_towers_[0].getPhysics(), towers_->at(i)->getPhysics(), NULL))
            {
                can_drop_tower = false;
                break;
            }
        }

        if (!armature_.isAnimating(&anim_drop_) && input_.grab && !last_input_.grab && getPosition().y < 0.9f && can_drop_tower)
        {
            armature_.loadAnimation(&anim_drop_, 0.15f);
        }
        return;
    }

    Tower *touching_tower = NULL;

    for (unsigned int i = 0; i < towers_->size(); i++)
    {
        if (world_->hasIntersected(towers_->at(i)->getPhysics(), &physics_, NULL))
        {
            touching_tower = towers_->at(i);
            break;
        }
    }

    if (touching_tower != NULL)
    {
        if (input_.grab && !last_input_.grab && !upgrade_menu_.isSelecting() && !build_menu_.isSelecting() && !has_created_a_tower_ && getPosition().y < 0.9f)
        {
            grabbing_tower_ = touching_tower;
            world_->removeObject(grabbing_tower_->getPhysics());
        }
    }
}

void Player::getInputAsKeyboard()
{
    cl::Keys keys = cl::Input::getKeyboardState();

    input_.move_dir = glm::vec2(0.0f);

    if (keys[SDL_SCANCODE_W])
        input_.move_dir.y += 1.0f;

    if (keys[SDL_SCANCODE_S])
        input_.move_dir.y -= 1.0f;

    if (keys[SDL_SCANCODE_D])
        input_.move_dir.x -= 1.0f;

    if (keys[SDL_SCANCODE_A])
        input_.move_dir.x += 1.0f;

    if (glm::length(input_.move_dir) > 0)
        input_.move_dir = glm::normalize(input_.move_dir);

    /*
    if (keys[SDL_SCANCODE_SPACE])
        std::cout << cl::toStringV3(getPosition()) << std::endl;
        */

    last_input_ = input_;

    input_.camera_left = keys[SDL_SCANCODE_LEFT];
    input_.camera_right = keys[SDL_SCANCODE_RIGHT];
    input_.grab = keys[SDL_SCANCODE_E];
    input_.jump = keys[SDL_SCANCODE_SPACE];
    input_.build_show = keys[SDL_SCANCODE_F];
    input_.build_next = keys[SDL_SCANCODE_R];
    input_.build_prev = keys[SDL_SCANCODE_E];
    input_.build_create = keys[SDL_SCANCODE_G];
    input_.upgrade_show = keys[SDL_SCANCODE_Q];
    input_.upgrade = keys[SDL_SCANCODE_E];
}

void Player::getInputAsController()
{
    last_input_ = input_;

    SDL_GameController *pad = cl::Input::getControllers()->at(0);
    input_.move_dir = cl::Input::getControllerLeftAxis(pad);
    
    if (glm::abs(input_.move_dir.x) < 0.2f)
        input_.move_dir.x = 0.0f;
        
    if (glm::abs(input_.move_dir.y) < 0.2f)
        input_.move_dir.y = 0.0f;
        
    if (glm::length(input_.move_dir) > 0.0f)
        input_.move_dir = glm::normalize(input_.move_dir);
    
    input_.move_dir *= -1.0f;

    input_.camera_left = cl::Input::getControllerRightAxis(pad).x < -0.2f;
    input_.camera_right = cl::Input::getControllerRightAxis(pad).x > 0.2f;
    input_.build_show = cl::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_Y);
    input_.build_next = cl::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_RIGHTSHOULDER);
    input_.build_prev = cl::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_LEFTSHOULDER);
    input_.build_create = cl::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_X);
    input_.grab = cl::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_X);
    input_.jump = cl::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_A);
    input_.upgrade_show = cl::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_B);
    input_.upgrade = cl::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_X);
}

void Player::processSummon(float dt)
{
    summoning_tick_ += dt;

    if (!isSummoning())
    {
        done_summon_ = false;
        summon_image_.setColour(glm::vec4(0.0f));

        if (!towers_->empty())
            towers_->back()->setActive(true);
    }
    else
    {
        if (!done_summon_)
        {
            SoundManager *sm = SoundManager::instance();
            summon_sound_ = sm->at(SPAWN);
            summon_sound_->play();
        }
        done_summon_ = true;
        summon_image_.setColour(glm::vec4(1.0f));
        summon_image_.setRotation(summon_image_.getRotation() + glm::vec3(0.0f, dt * 6.0f, 0.0f));
        summon_image_.updateLastPosition();
        summon_image_.setPosition(glm::vec3(towers_->back()->getPosition().x, summon_image_.getPosition().y, towers_->back()->getPosition().z), false);

        float alpha = summoning_tick_ / summoning_time_;

        towers_->back()->setModelOffset(glm::vec3(0.0f, ((1.0f - alpha) * -5.0f) - (1.65 * 0.5f), 0.0f));
    }
}

void Player::input(cl::Camera *camera, float dt)
{
    if (cl::Input::getControllers()->empty())
        getInputAsKeyboard();
    else
        getInputAsController();

    processInput(dt);

    float camera_x = 13.0f * std::cos(camera_rotation_);
    float camera_z = 13.0f * std::sin(camera_rotation_);

    camera->setPositionOffset(glm::vec3(camera_x, 10.0f, camera_z));

    camera_direction_ = glm::normalize(getPosition() - (camera->getPosition() + camera->getPositionOffset()));

    if (!isSummoning())
        cl::Entity3D::input(input_.move_dir, camera, input_.jump, dt);
    else
        cl::Entity3D::input(cl::Entity3D::getInputDirection(), camera, input_.jump, dt);
}

void Player::processInput(float dt)
{
    if (input_.camera_left)
        camera_rotation_ -= 2.5f * dt;

    if (input_.camera_right)
        camera_rotation_ += 2.5f * dt;

    if (input_.upgrade_show && !last_input_.upgrade_show && !upgrade_menu_.isSelecting() && !build_menu_.isSelecting() && !isSummoning() && grabbing_tower_ == NULL)
    {
        Tower *touch_tower = NULL;

        for (unsigned int i = 0; i < towers_->size(); i++)
        {
            if (world_->hasIntersected(towers_->at(i)->getPhysics(), getPhysics(), NULL))
            {
                touch_tower = towers_->at(i); 
                break;
            }
        }

        if (touch_tower != NULL)
        {
            upgrade_menu_.show(touch_tower);
            upgrade_location_ = getPosition();
        }
    }
    else if (input_.upgrade_show && !last_input_.upgrade_show && upgrade_menu_.isSelecting() && !isSummoning())
        upgrade_menu_.cancel();

    if (input_.build_show && !last_input_.build_show && !build_menu_.isSelecting() && !upgrade_menu_.isSelecting() && !isSummoning() && grabbing_tower_ == NULL)
        build_menu_.show();

    else if (input_.build_show && !last_input_.build_show && build_menu_.isSelecting() && !isSummoning())
        build_menu_.cancel();

    if (input_.build_next && !last_input_.build_next && build_menu_.isSelecting())
        build_menu_.next();

    if (input_.build_prev && !last_input_.build_prev && build_menu_.isSelecting())
        build_menu_.previous();

    has_created_a_tower_ = false;

    if (input_.build_create && !last_input_.build_create && build_menu_.isSelecting() && !isSummoning() && can_place_tower_)
    {
        TOWERS tower = build_menu_.getSelection();
        has_created_a_tower_ = createTower(tower);
        build_menu_.cancel();
    }

    if (input_.upgrade && !last_input_.upgrade && upgrade_menu_.isSelecting() && !isSummoning())
    {
        if (upgrade_menu_.getSelectedTower()->getLevel() < 3 && crystals_ >= upgrade_menu_.getSelectedTower()->getCost())
        {
            crystals_ -= upgrade_menu_.getSelectedTower()->getCost();
            upgrade_menu_.upgrade();
        }
    }
}

void Player::animations(float dt)
{
    Tower *touching_tower = NULL;

    for (unsigned int i = 0; i < towers_->size(); i++)
    {
        if (world_->hasIntersected(towers_->at(i)->getPhysics(), &physics_, NULL))
        {
            touching_tower = towers_->at(i);
            break;
        }
    }

    if (isSummoning())
    {
        if (!armature_.isAnimating(&anim_idle_))
            armature_.loadAnimation(&anim_idle_, 0.15f);
    }
    else if (armature_.isAnimating(&anim_jump_))
    {
        if (physics_.getVelocity().y < 0)
        {
            armature_.loadAnimation(&anim_fall_, 0.15f);
        }
    }
    else if (armature_.isAnimating(&anim_fall_))
    {
        if (physics_.onGround())
        {
            armature_.loadAnimation(&anim_idle_, 0.15f);
        }
    }
    else if (input_.jump && grabbing_tower_ == NULL && !armature_.isAnimating(&anim_jump_))
    {
        armature_.loadAnimation(&anim_jump_, 0.15f);
    }

    else if (armature_.isAnimating(&anim_drop_))
    {
        if (anim_drop_.finished())
        {
            SoundManager* sm = SoundManager::instance();
            sm->at(DROP)->play();
            world_->addObject(grabbing_tower_->getPhysics(), COL_TOWER, COL_ENVIRONMENT | COL_PLAYER | COL_ENEMY | COL_TOWER);
            grabbing_tower_->getPhysics()->getRigidBody()->activate(true);
            grabbing_tower_ = NULL;
            armature_.loadAnimation(&anim_idle_, 0.15f);
        }
    }
    else if (grabbing_tower_ != NULL)
    {
        if (glm::length(getInputDirection()) > 0.01f)
        {
            if (!armature_.isAnimating(&anim_run_holding_))
                armature_.loadAnimation(&anim_run_holding_, 0.15f);
        }
        else
        {
            bool was_running = armature_.isAnimating(&anim_run_holding_);
            if (!armature_.isAnimating(&anim_lift_))
            {
                armature_.loadAnimation(&anim_lift_, 0.15f);
                
                if (!was_running)
                {
                    // SoundManager* sm = SoundManager::instance();
                    // sm->at(LIFT)->play();
                }

                if (was_running)
                {
                    anim_lift_.setTick(anim_lift_.getDuration() + 1.0f);
                    anim_lift_.update(0.0f);
                }
            }
        }
    }
    else if (glm::length(getInputDirection()) > 0.01f && grabbing_tower_ == NULL)
    {
        if (touching_tower == NULL)
        {
            if (!armature_.isAnimating(&anim_run_))
                armature_.loadAnimation(&anim_run_, 0.15f);
        }
        else
        {
            glm::vec2 fd = getFacingDirection();
            glm::vec3 facing_3d(fd.x, 0.0f, fd.y);
            facing_3d = glm::normalize(facing_3d);
        
            glm::vec3 tower_dir = touching_tower->getPosition() - getPosition();
            tower_dir.y = 0.0f;
            tower_dir = glm::normalize(tower_dir);
        
            bool facing = glm::dot(tower_dir, facing_3d) > 0.8f;
        
            if (!armature_.isAnimating(&anim_push_) && facing)
            {
                armature_.loadAnimation(&anim_push_, 0.15f);

            }
        }
    }
    else if (grabbing_tower_ == NULL && glm::length(getInputDirection()) <= 0.01f && !armature_.isAnimating(&anim_idle_))
    {
        armature_.loadAnimation(&anim_idle_, 0.15f);
    }
    
    if (armature_.isAnimating(&anim_push_) && touching_tower != NULL)
    {
        if (anim_push_.getTick() > 0.5 && !play_push_)
        {
            SoundManager* sm = SoundManager::instance();
            sm->at(SLIDE)->play();
            play_push_ = true;
        }
    }
    else 
        play_push_ = false;
    

    armature_.update(dt);
}

bool Player::createTower(TOWERS type)
{
    if (isSummoning())
        return false;

    switch (type)
    {
    case WALL: summon_tower_category_ = "TowerShield"; break;
    case ICE: summon_tower_category_ = "TowerIce"; break;
    case FIRE: summon_tower_category_ = "TowerFire"; break;
    case CRYSTAL: summon_tower_category_ = "TowerCrystal"; break;
    case POISON: summon_tower_category_ = "TowerPoison"; break;
    case LIGHTNING: summon_tower_category_ = "TowerLightning"; break;
    default: summon_tower_category_ = "TowerCrystal"; break;
    }

    summon_location_ = getCreateTowerPosition();
    
    Tower *tower = new Tower();
    tower->initialize(&tower_settings_, summon_tower_category_, tower_shader_, &tower_cache_, particle_shader_, type, bullet_shader_);
    // check if enough resources
    if (crystals_ < tower->getCost()) {
        std::cout << "Not enough resources: have " << crystals_ << " but need " << tower->getCost() << std::endl;
        delete tower;
        return false;
    }
    // spend crystals
    crystals_ -= tower->getCost();
    // set summoning animation
    summoning_tick_ = 0.0f;
    summoning_time_ = 3.0f;
    // create tower
    tower->setPosition(summon_location_, true);
    tower->setModelOffset(glm::vec3(0.0f, -5.0f, 0.0f));
    world_->addObject(tower->getPhysics(), COL_TOWER, COL_ENVIRONMENT | COL_PLAYER | COL_ENEMY | COL_TOWER);
    towers_->push_back(tower);

    summon_image_.setPosition(summon_location_ - glm::vec3(0.0f, 0.5f, 0.0f), true);

    return true;
}

bool Player::isSummoning() const
{
    return summoning_tick_ < summoning_time_;
}

cl::BulletCharacterObject* Player::getPhysics()
{
    return &physics_;
}

cl::Image* Player::getSummonImage()
{
    return &summon_image_;
}

cl::Image* Player::getBuildMenuImage()
{
    return build_menu_.isSelecting() ? build_menu_.getSelectedImage() : NULL;
}

cl::Image* Player::getUpgradeMenuImage()
{
    return upgrade_menu_.isSelecting() ? upgrade_menu_.getSelectedImage() : NULL;
}

cl::Text* Player::getBuildMenuCostText()
{
    return build_menu_.isSelecting() ? build_menu_.getCostText() : NULL;
}

cl::Text* Player::getUpgradeMenuCostText()
{
    return upgrade_menu_.isSelecting() ? upgrade_menu_.getCostText() : NULL;
}

Tower* Player::getSelectedSampleTower()
{
    if (build_menu_.isSelecting())
    {
        return &sample_towers_[build_menu_.getSelection()];
    }

    return NULL;
}

glm::vec3 Player::getCreateTowerPosition() const
{
    glm::vec2 facing_dir = getFacingDirection();
    glm::vec3 facing_dir_3d(facing_dir.x, 0.0f, facing_dir.y);
    facing_dir_3d = glm::normalize(facing_dir_3d);
    return getPosition() + 2.0f * facing_dir_3d;
}

void Player::addCrystals(float crystals) {
    crystals_ += crystals;
}

float Player::getCrystals() const
{
    return crystals_;
}

