#include "Engine.hpp"

Engine::Engine()
{
    if (!initialize())
        return;

    createWindow("GGJ", 1280, 720, 4);

    // Initialize Shaders

    cl::AssetLoader loader;

    cl::File *mr_vs = loader.loadFile("data/shaders/model_rigged_vs.glsl", cl::Loader::NONE, NULL);
    cl::File *mr_fs = loader.loadFile("data/shaders/model_rigged_fs.glsl", cl::Loader::NONE, NULL);
    cl::File *ms_vs = loader.loadFile("data/shaders/model_static_vs.glsl", cl::Loader::NONE, NULL);
    cl::File *ms_fs = loader.loadFile("data/shaders/model_static_fs.glsl", cl::Loader::NONE, NULL);
    cl::File *p_vs = loader.loadFile("data/shaders/particle_emitter_vs.glsl", cl::Loader::NONE, NULL);
    cl::File *p_fs = loader.loadFile("data/shaders/particle_emitter_fs.glsl", cl::Loader::NONE, NULL);
    cl::File *i_vs = loader.loadFile("data/shaders/image_vs.glsl", cl::Loader::NONE, NULL);
    cl::File *i_fs = loader.loadFile("data/shaders/image_fs.glsl", cl::Loader::NONE, NULL);
    cl::File *ib_vs = loader.loadFile("data/shaders/image_billboard_vs.glsl", cl::Loader::NONE, NULL);
    cl::File *ib_fs = loader.loadFile("data/shaders/image_billboard_fs.glsl", cl::Loader::NONE, NULL);
    cl::File *bd_vs = loader.loadFile("data/shaders/bullet_debug_vs.glsl", cl::Loader::NONE, NULL);
    cl::File *bd_fs = loader.loadFile("data/shaders/bullet_debug_fs.glsl", cl::Loader::NONE, NULL);

    cl::File *bullet_vs = loader.loadFile("data/shaders/bullet_vs.glsl", cl::Loader::NONE, NULL);
    cl::File *bullet_fs = loader.loadFile("data/shaders/bullet_fs.glsl", cl::Loader::NONE, NULL);

    cl::Shader::appendGLSLVersion(&mr_vs->contents);
    cl::Shader::appendGLSLVersion(&mr_fs->contents);
    cl::Shader::appendGLSLVersion(&ms_vs->contents);
    cl::Shader::appendGLSLVersion(&ms_fs->contents);
    cl::Shader::appendGLSLVersion(&p_vs->contents);
    cl::Shader::appendGLSLVersion(&p_fs->contents);
    cl::Shader::appendGLSLVersion(&i_vs->contents);
    cl::Shader::appendGLSLVersion(&i_fs->contents);
    cl::Shader::appendGLSLVersion(&ib_vs->contents);
    cl::Shader::appendGLSLVersion(&ib_fs->contents);
    cl::Shader::appendGLSLVersion(&bd_vs->contents);
    cl::Shader::appendGLSLVersion(&bd_fs->contents);

    cl::Shader::appendGLSLVersion(&bullet_vs->contents);
    cl::Shader::appendGLSLVersion(&bullet_fs->contents);

    shaders_.model_rigged.compileVertexShader(mr_vs->contents);
    shaders_.model_rigged.compileFragmentShader(mr_fs->contents);
    shaders_.model_rigged.bindAttribLocation("vPosition", 0);
    shaders_.model_rigged.bindAttribLocation("vTexCoord", 1);
    shaders_.model_rigged.bindAttribLocation("vWeights", 2);
    shaders_.model_rigged.bindAttribLocation("vBoneids", 3);
    shaders_.model_rigged.linkProgram();
    shaders_.model_rigged.setUniformI("tex_sample", 0);

    shaders_.model_static.compileVertexShader(ms_vs->contents);
    shaders_.model_static.compileFragmentShader(ms_fs->contents);
    shaders_.model_static.bindAttribLocation("vPosition", 0);
    shaders_.model_static.bindAttribLocation("vTexCoord", 1);
    shaders_.model_static.linkProgram();
    shaders_.model_static.setUniformI("tex_sample", 0);

    shaders_.image.compileVertexShader(i_vs->contents);
    shaders_.image.compileFragmentShader(i_fs->contents);
    shaders_.image.bindAttribLocation("vPosition", 0);
    shaders_.image.bindAttribLocation("vTexCoord", 1);
    shaders_.image.linkProgram();
    shaders_.image.setUniformI("tex_sample", 0);

    shaders_.particles.compileVertexShader(p_vs->contents);
    shaders_.particles.compileFragmentShader(p_fs->contents);
    shaders_.particles.bindAttribLocation("vPosition", 0);
    shaders_.particles.bindAttribLocation("vColour", 1);
    shaders_.particles.linkProgram();

    shaders_.billboard.compileVertexShader(ib_vs->contents);
    shaders_.billboard.compileFragmentShader(ib_fs->contents);
    shaders_.billboard.bindAttribLocation("vPosition", 0);
    shaders_.billboard.bindAttribLocation("vTexCoord", 1);
    shaders_.billboard.linkProgram();
    shaders_.billboard.setUniformI("tex_sample", 0);

    shaders_.bullet_debug.compileVertexShader(bd_vs->contents);
    shaders_.bullet_debug.compileFragmentShader(bd_fs->contents);
    shaders_.bullet_debug.bindAttribLocation("vPosition", 0);
    shaders_.bullet_debug.linkProgram();

    shaders_.bullet.compileVertexShader(bullet_vs->contents);
    shaders_.bullet.compileFragmentShader(bullet_fs->contents);
    shaders_.bullet.bindAttribLocation("vPosition", 0);
    shaders_.bullet.bindAttribLocation("vTexCoord", 1);
    shaders_.bullet.linkProgram();
    shaders_.bullet.setUniformI("tex_sample", 0);

    delete mr_vs;
    delete mr_fs;
    delete ms_vs;
    delete ms_fs;
    delete p_vs;
    delete p_fs;
    delete i_vs;
    delete i_fs;
    delete ib_vs;
    delete ib_fs;
    delete bd_vs;
    delete bd_fs;
    delete bullet_vs;
    delete bullet_fs;

    setMaxAudioChannels(512);
    level_.initialize(&shaders_);
}

void Engine::events(const SDL_Event &event)
{
    if (event.type == SDL_QUIT)
        cl::Engine::killGameLoop();
}

void Engine::logic(float dt)
{
    level_.update(dt);
}

void Engine::render(cl::Renderer *renderer)
{
    level_.draw(renderer);
}

