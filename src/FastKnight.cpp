#include "FastKnight.hpp"

void FastKnight::initialize(cl::Shader *shader) {
    cl::AssetLoader loader;

    // stat initializaiton
    reward_ = 110 + (10 * multiplier_);
    health_ = 80 + (7.5 * multiplier_);
    movespeed_ = 2.5f + (0.15f * multiplier_);
    model_data_ = loader.loadModelData("data/fastknight/fast-knight.cm", cl::Loader::NO_NORMALS, &cache_);

    cl::SkeletalAnimation *anim_run = loader.loadSkeletalAnimation("data/fastknight/fast-knight-run.ca", cl::Loader::NONE, &cache_, true);
    cl::SkeletalAnimation *anim_die = loader.loadSkeletalAnimation("data/fastknight/fast-knight-death.ca", cl::Loader::NONE, &cache_, false);

    anim_run_ = *anim_run;
    anim_die_ = *anim_die;
    armature_ = *model_data_->getArmature();

    armature_.loadAnimation(&anim_run_, 0.0f);

    cl::Entity3D::initialize(model_data_, shader, &physics_);

    cl::Entity3D::setPhysicsRotation(false);
    cl::Entity3D::setTurnSpeed(8.0f);
    cl::Entity3D::setMoveSpeed(movespeed_);
    cl::Entity3D::setJumpSpeed(20.0f);
    cl::Entity3D::setModelOffset(glm::vec3(0.0f, -1.65f * 0.5f, 0.0f));

    physics_.setTriggerPointer(this);
    physics_.setTriggerIndex(1);

    Enemy::initialize(shader);
}

FastKnight::FastKnight(float multiplier) :
    Enemy(),
    physics_shape_(0.4f * 0.5f, 1.65f * 0.5f),
    physics_(&physics_shape_, 1.65f * 0.5f, 10.0f, glm::quat(glm::vec3(0.0f)), glm::vec3(0.0f, 4.0f, 0.0f))
{
    multiplier_ = multiplier;
}

FastKnight::FastKnight(std::vector<glm::vec3> waypoints, float multiplier) :
    Enemy(waypoints),
    physics_shape_(0.4f * 0.5f, 1.65f * 0.5f),
    physics_(&physics_shape_, 1.65f * 0.5f, 10.0f, glm::quat(glm::vec3(0.0f)), glm::vec3(0.0f, 4.0f, 0.0f))
{
    multiplier_ = multiplier;
}

cl::BulletCharacterObject* FastKnight::getPhysics()
{
    return &physics_;
}

void FastKnight::update(cl::Camera *camera, float dt)
{
    Enemy::update(camera, dt);

    if (health_ < 1)
    {
        die();

        if (!armature_.isAnimating(&anim_die_))
            armature_.loadAnimation(&anim_die_, 0.15f);

        cl::Entity3D::setMoveSpeed(0);
    }

    armature_.update(dt);
    cl::Entity3D::update(dt);
}

void FastKnight::takeDamage(float damage) {
    // apply defensive multipliers here
    Enemy::takeDamage(damage);
}
