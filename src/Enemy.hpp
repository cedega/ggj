#pragma once
#include <Clone/3D/Entity3D.hpp>
#include <Clone/3D/Armature.hpp>
#include <Clone/Bullet/BulletCharacterObject.hpp>
#include <Clone/AssetLoader.hpp>
#include <Clone/Input.hpp>
#include <vector>
#include <iostream>
#include "Buff.hpp"
#include "SoundManager.hpp"


class Enemy : public cl::Entity3D
{
    public:
        Enemy();
        Enemy(std::vector<glm::vec3> waypoints);

        void uploadToShader(cl::Shader *shader, float interpolation, int draw_index);

        virtual void initialize(cl::Shader *shader);
        virtual void update(cl::Camera *camera, float dt);

        virtual cl::BulletCharacterObject* getPhysics() = 0;
        virtual void takeDamage(float damage);
        void setWaypoint(std::vector<glm::vec3> waypoints);
        void die();
        bool isAlive() const;
        bool destroy();
        float getReward() const;
        bool noMoreWaypoints() const;
        Buff getBuff() const;
        void setBuff(Buff buff);

    protected:
        bool alive_;
        float health_;
        float reward_;
        float movespeed_;
        Buff buff_;
        float multiplier_;
        
        cl::Armature armature_;
        static cl::AssetCache cache_;
    private:
        cl::SkeletalAnimation anim_walk_;
        size_t cur_wp_index_;
        std::vector<glm::vec3> waypoints_;

        void updateBuff(float dt);
        void input(cl::Camera *camera, float dt);
        void animations(float dt);
        virtual void ai(void);
        glm::vec2 faceWaypoint();
        bool hasReachedWaypoint();
        int nextWaypoint();
};

