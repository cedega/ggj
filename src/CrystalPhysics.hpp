#pragma once
#include <Clone/PhysicsObject.hpp>

class CrystalPhysics : public cl::PhysicsObject
{
 public:
    CrystalPhysics();

    void update(float dt);

    void setPosition(const glm::vec3 &position);
    glm::vec3 getPosition() const;

    void setRotation(const glm::vec3 &rotation);
    glm::vec3 getRotation() const;

    void setVelocity(const glm::vec3 &velocity);
    glm::vec3 getVelocity() const;

 private:
    glm::vec3 position_;
    glm::vec3 rotation_;
    glm::vec3 velocity_;
};
