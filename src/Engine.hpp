#pragma once
#include <Clone/Engine.hpp>
#include <Clone/AssetLoader.hpp>
#include "Level.hpp"

class Engine : public cl::Engine
{
 public:
    Engine();

    void events(const SDL_Event &event);
    void logic(float dt);
    void render(cl::Renderer *renderer);

 private:
    ShaderPack shaders_;
    Level level_;
};
