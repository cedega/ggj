#include "Ritualist.hpp"

cl::AssetCache Ritualist::cache_;

Ritualist::Ritualist(glm::vec3 pos) :
    physics_shape_(0.2f, 0.875f),
    physics_(&physics_shape_, 0.875f, 10.0f, glm::quat(glm::vec3(0.0f)), glm::vec3(0.0f, 4.0f, 0.0f)),
    pos_(pos)
{
}

Ritualist::~Ritualist()
{
}

void Ritualist::uploadToShader(cl::Shader *shader, float interpolation, int draw_index)
{
    cl::Entity3D::uploadToShader(shader, interpolation, draw_index);
    shader->setUniformM44v("bones[0]", &armature_.getMatrixTransforms()[0], armature_.getMatrixTransforms().size());
}

void Ritualist::initialize(cl::Shader *shader)
{
    cl::AssetLoader loader;
    model_data_ = loader.loadModelData("data/acolyte/acolyte.cm", cl::Loader::NO_NORMALS, &cache_);

    cl::SkeletalAnimation *anim = loader.loadSkeletalAnimation("data/acolyte/acolyte.ca", cl::Loader::NONE, &cache_, true);
    anim_ = *anim;

    armature_ = *model_data_->getArmature();
    armature_.loadAnimation(&anim_, 0.0f);

    cl::Entity3D::initialize(model_data_, shader, &physics_);

    cl::Entity3D::setPhysicsRotation(false);
    cl::Entity3D::setTurnSpeed(0.0f);
    cl::Entity3D::setMoveSpeed(0.0f);
    cl::Entity3D::setJumpSpeed(0.0f);
    cl::Entity3D::setModelOffset(glm::vec3(0.0f, -1.65f * 0.5f, 0.0f));

    cl::Entity3D::setCullAABB(cl::AABB(glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(1.5f, 2.0f, 1.5f)));
    cl::Entity3D::setPosition(pos_, false);
}

void Ritualist::update(cl::Camera *camera, float dt) {
    input(camera, dt);
    animations(dt);

    cl::Entity3D::update(dt);
}

void Ritualist::input(cl::Camera *camera, float dt) {
    cl::Entity3D::input(glm::vec2(0.0f), camera, false, dt);
}

void Ritualist::animations(float dt)
{
    armature_.update(dt);
}

cl::BulletCharacterObject* Ritualist::getPhysics() {
    return &physics_;
}
