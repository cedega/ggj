#pragma once
#include <vector>
#include <Clone/Renderer.hpp>
#include <Clone/Bullet/BulletPhysicsWorld.hpp>
#include <Clone/Text.hpp>
#include <Clone/Music.hpp>
#include <iostream>
#include <string>
#include "Player.hpp"
#include "Tower.hpp"
#include "Knight.hpp"
#include "BossKnight.hpp"
#include "Ritualist.hpp"
#include "CollisionMask.hpp"
#include "Crystal.hpp"
#include "BuildMenu.hpp"
#include "SoundManager.hpp"
#include <Clone/Bullet/BulletCollisions.hpp>

enum Music {
    INTRO,
    LOOP,
    AMBIENT,
    REQUIEM
};


struct ShaderPack
{
    cl::Shader model_rigged;
    cl::Shader model_static; 
    cl::Shader image;
    cl::Shader particles;
    cl::Shader billboard;
    cl::Shader bullet_debug;
    cl::Shader bullet;
};

struct Wave
{
    int level;
    float spawn_time;
    int num_knights_enemies;
    int num_fast_knights_enemies;
    int num_boss_enemies;
    float wave_time;
    int wp_index;
    float enemy_scale;
    int enemies_spawned;
    float wave_timer;
    float spawn_timer;

    Wave(int l, float s, int n, int n1, int n2, float t, int wp, float es) :
        level(l), spawn_time(s), num_knights_enemies(n), num_fast_knights_enemies(n1), num_boss_enemies(n2), wave_time(t), wp_index(wp), enemy_scale(es),
        enemies_spawned(0), wave_timer(0.0f), spawn_timer(0)
    {}
};

class Level
{
 public:
    Level();
    ~Level();

    void initialize(ShaderPack *shaders);
    void update(float dt);
    void draw(cl::Renderer *renderer);

 private:
    cl::Camera camera_;
    cl::Camera camera_2d_;

    cl::BulletPhysicsWorld world_;
    cl::BulletCollisions collisions_;
    cl::AssetCache cache_2d_;

    cl::ModelData *level_model_data_;
    cl::Model level_model_;
    ShaderPack *sp;

    Player player_;
    std::vector<Enemy*> enemies_;
    std::vector<Tower*> towers_;

    std::vector<Crystal*> crystals_;
    cl::AssetCache crystal_cache_;

    cl::Font *font_;
    cl::Text currency_text_;
    cl::Text wave_text_;
    cl::Text lives_text_;

    cl::Image currency_bg_;
    cl::Image wave_bg_;
    cl::Image lives_bg_;

    std::vector<std::vector<glm::vec3> > waypoints_;
    std::vector<Wave*> waves_;

    std::vector<Ritualist*> ritualists_;
    btStaticPlaneShape ground_shape_;
    cl::BulletPhysicsObject ground_;

    size_t cur_wave_index_;
    std::vector<cl::Music*> music_;
    std::vector<cl::Sound*> sound_;
    cl::GameSettings wave_settings_;

    void processWave(Wave* wave, float dt);
    void initWaves(void);

    bool getClosestEnemyLocation(Tower *tower, glm::vec3 *output);
    bool finished_;
    bool finished_good_;
    cl::Image finalImage_;
    cl::Image finalImageGood_;

};
