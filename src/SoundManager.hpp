#pragma once
#include <Clone/AssetLoader.hpp>
#include <Clone/Sound.hpp>

class Player;


enum Sound {
    DEATH,
    SLIDE,
    PICKUP,
    FIRE_CAST,
    FIRE_HIT,
    LIGHTNING_CAST,
    LIGHTNING_HIT,
    POISON_CAST,
    POISON_HIT,
    ICE_CAST,
    ICE_HIT,
    CRYSTAL_CAST,
    DROP,
    SPAWN,
    RITUALIST,
    LIFT,
    UPGRADE,
    CRUMBLE
};

#define SOUND_FALLOFF 10.0f

class SoundManager {
    public:
        static SoundManager* instance();
        cl::Sound* at(unsigned int i);
        void setPlayer(Player* p);
        Player* getPlayer();
    private:
        SoundManager();
        static SoundManager *inst;
        Player* player; // fuck isaac no love no underscores
        std::vector<cl::Sound*> sound_;
};

