#include "Tower.hpp"
#include "Player.hpp"

void deathparticles(cl::Particle *particle, int index, void *data)
{
    const float tick = 360.0f / 500.0f;
    const float u = index * tick;
    const float v = cl::randomF(0.0f, 180.0f);

    particle->life_time = cl::randomF(3.0f, 4.0f);

    particle->position.x = cl::randomF(0.5f, 1.0f)*std::cos(glm::radians(u))*std::sin(glm::radians(v)); 
    particle->position.y = cl::randomF(0.5f, 1.0f)*std::cos(glm::radians(v));
    particle->position.z = cl::randomF(0.5f, 1.0f)*std::sin(glm::radians(u))*std::sin(glm::radians(v));

    particle->colour = glm::vec4(0.0f, 0.0f, 0.0f, 0.6f);

    particle->velocity = particle->position;
    particle->acceleration = -0.2f * particle->velocity;
}

Tower::Tower() :
    alive_(true),
    active_(false),
    physics_shape_(btVector3(1.65f * 0.5f, 1.65f * 0.5f, 1.65f * 0.5f)),
    physics_(&physics_shape_, 100.0f, glm::quat(glm::vec3(0.0f)), glm::vec3(0.0f, 3.0f, 0.0f))
{

}

Tower::~Tower()
{
    for (unsigned int i = 0; i < bullets_.size(); i++)
        delete bullets_[i];
}

void Tower::uploadToShader(cl::Shader *shader, float interpolation, int draw_index)
{
    cl::Entity3D::uploadToShader(shader, interpolation, draw_index);

    //shader->setUniformM44v("bones[0]", &armature_.getMatrixTransforms()[0], armature_.getMatrixTransforms().size());
}

void Tower::initialize(cl::GameSettings *gs, const std::string &tower_cat, cl::Shader *shader, cl::AssetCache *cache, cl::Shader *particle_shader, int type, cl::Shader *bullet_shader)
{
    level_ = 1;
    type_ = type;
    settings_ = gs;
    tower_cat_ = tower_cat;
    model_shader_ = shader;
    particle_shader_ = particle_shader;
    bullet_shader_ = bullet_shader;
    // set statistics from file
    std::string model_path = gs->getStringKey(tower_cat, "ModelPath");
    std::string anim_path = gs->getStringKey(tower_cat, "AnimPath");
    cost_ = gs->getFloatKey(tower_cat, "Cost");
    damage_ = gs->getFloatKey(tower_cat, "Damage");
    buff_.slow = gs->getIntKey(tower_cat, "Slow");
    buff_.dot = gs->getIntKey(tower_cat, "Dot");
    buff_.length = gs->getFloatKey(tower_cat, "Length");
    buff_.tickdmg = gs->getFloatKey(tower_cat, "Tickdmg");

    aoe_ = gs->getFloatKey(tower_cat, "AoE");
    range_ = gs->getFloatKey(tower_cat, "AtkRng");
    cost_up_ = gs->getFloatKey(tower_cat, "Cost_UP");
    damage_up_ = gs->getFloatKey(tower_cat, "Damage_UP");
    length_up_ = gs->getFloatKey(tower_cat, "Length_UP");
    tickdmg_up_ = gs->getFloatKey(tower_cat, "Tickdmg_UP");
    range_up_ = gs->getFloatKey(tower_cat, "AtkRng_UP");
    rate_up_ = gs->getFloatKey(tower_cat, "AtkRate_UP");
    fire_time_ = gs->getFloatKey(tower_cat, "AtkRate");

    mass_ = gs->getFloatKey(tower_cat, "Mass");
    mass_up_ = gs->getFloatKey(tower_cat, "Mass_UP");

    cl::AssetLoader loader;
    cl::ModelData *model_data = loader.loadModelData(model_path, cl::Loader::NO_NORMALS, cache);
    armature_ = *model_data->getArmature();

    cl::SkeletalAnimation *anim = loader.loadSkeletalAnimation(anim_path, cl::Loader::NO_NORMALS, cache, true);
    animation_ = *anim;

    // armature_.loadAnimation(&animation_, 0.0f);

    cl::Entity3D::initialize(model_data, shader, &physics_); 
    cl::Entity3D::setModelOffset(glm::vec3(0.0f, -1.65f * 0.5f, 0.0f));
    cl::Entity3D::setCullAABB(cl::AABB(glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(1.5f, 2.0f, 1.5f)));
    cl::Entity3D::setColour(glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
    cl::Entity3D::setBackfaceCulling(false);

    physics_.setAngularFactor(glm::vec3(0.0f));
    physics_.setMass(mass_);

    death_particles_.create(8000, deathparticles, NULL, particle_shader);
    death_particles_.setPointSize(2.0f);

    // Properties
    fire_tick_ = 0.0f;
}

void Tower::update(float dt)
{
    fire_tick_ += dt;

    death_particles_.update(dt);
    armature_.update(dt);
    cl::Entity3D::update(dt);
}

void Tower::kill()
{
    alive_ = false;
    death_particles_.setPosition(getPosition(), true);
    death_particles_.play();
}

bool Tower::isAlive() const
{
    return alive_;
}

bool Tower::destroy() const
{
    return !isAlive() && !death_particles_.isPlaying();
}

void Tower::shoot(const glm::vec3 &location, const glm::vec3 &direction, cl::BulletPhysicsWorld *world)
{
    if (fire_tick_ < fire_time_ + (level_ - 1) * rate_up_ || !isActive() || !isAlive())
        return;

    Bullet *bullet = new Bullet();
    bullet->initialize(settings_, tower_cat_, location, direction, bullet_shader_, particle_shader_, getType());
    world->addObject(bullet->getPhysics(), COL_BULLET, COL_ENEMY);
    bullet->setAoE(aoe_);
    bullets_.push_back(bullet);

    SoundManager* sm = SoundManager::instance(); 
    if (glm::distance(sm->getPlayer()->getPosition(),  getPosition()) < SOUND_FALLOFF) {
        switch (getType()) {
            case WALL:
            case ICE: sm->at(ICE_CAST)->play(); break;
            case LIGHTNING: sm->at(LIGHTNING_CAST)->play(); break;
            case FIRE: sm->at(FIRE_CAST)->play(); break;
            case POISON: sm->at(POISON_CAST)->play(); break;
            case CRYSTAL: sm->at(CRYSTAL_CAST)->play(); break;
        }
    }

    fire_tick_ = 0.0f;
}

void Tower::upgrade()
{
    level_++;

    physics_.setMass(mass_ + (level_ - 1) * mass_up_);
}

void Tower::setActive(bool active)
{
    active_ = active;
}

bool Tower::isActive() const
{
    return active_;
}

int Tower::getType() const
{
    return type_;
}

cl::BulletPhysicsObject* Tower::getPhysics()
{
    return &physics_;
}

cl::ParticleEmitter* Tower::getParticles()
{
    return &death_particles_;
}

std::vector<Bullet*>* Tower::getBullets()
{
    return &bullets_;
}

int Tower::getLevel() const
{
    return level_;
}

float Tower::getCost() const {
    return cost_ + (level_ - 1) * cost_up_;
}

float Tower::getDamage() const {
    return damage_ + (level_ - 1) * damage_up_;
}

Buff Tower::getBuff() const
{
    Buff temp = buff_;
    temp.length += (level_ - 1) * length_up_;
    temp.tickdmg += (level_ - 1) * tickdmg_up_;
    return temp;
}

float Tower::getAttackRange() const
{
    return range_ + (level_ - 1) * range_up_;
}

