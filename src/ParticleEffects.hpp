#pragma once
#include <Clone/ParticleEmitter.hpp>

void ice_flying(cl::Particle *particle, int index, void *data);
void ice_impact(cl::Particle *particle, int index, void *data);

void fire_flying(cl::Particle *particle, int index, void *data);
void fire_impact(cl::Particle *particle, int index, void *data);

void elec_flying(cl::Particle *particle, int index, void *data);
void elec_impact(cl::Particle *particle, int index, void *data);

void dark_flying(cl::Particle *particle, int index, void *data);
void dark_impact(cl::Particle *particle, int index, void *data);

void poison_flying(cl::Particle *particle, int index, void *data);
void poison_impact(cl::Particle *particle, int index, void *data);

