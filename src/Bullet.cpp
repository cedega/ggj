#include "Bullet.hpp"
#include <iostream>

cl::AssetCache Bullet::bullet_cache_;

Bullet::Bullet() :
    shape_(0.001f),
    hitbox_shape_(0.3f),
    alive_(true),
    life_time_(1.5f),
    life_tick_(0.0f),
    aoe_(0.0f),
    hitbox_(&hitbox_shape_, 0.0f, glm::quat(glm::vec3(0.0f)), glm::vec3(0.0f)),
    physics_(&shape_, 5.0f, glm::quat(glm::vec3(0.0f)), glm::vec3(0.0f))

{
    physics_.setGravity(glm::vec3(0.0f));
}

void Bullet::initialize(cl::GameSettings *gs, const std::string &bullet_cat, const glm::vec3 &position, const glm::vec3 &dir, cl::Shader *shader, cl::Shader *particle_shader, int type)
{
    std::string model_path = gs->getStringKey(bullet_cat, "ProjPath");

    cl::AssetLoader loader;
    cl::ModelData *model_data = loader.loadModelData(model_path, cl::Loader::NO_NORMALS, &bullet_cache_);

    cl::Entity3D::initialize(model_data, shader, &physics_); 
    cl::Entity3D::setModelOffset(glm::vec3(0.0f, 0.0f, 0.0f));
    cl::Entity3D::setCullAABB(cl::AABB(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f)));
    cl::Entity3D::setPosition(position, true);
    cl::Entity3D::setPhysicsRotation(false);

    void (*flying)(cl::Particle*, int, void*) = NULL;
    void (*impact)(cl::Particle*, int, void*) = NULL;

    switch (type)
    {
    case 0: break;

    case 1:
        flying = ice_flying;
        impact = ice_impact;
        break;

    case 3:
        flying = fire_flying;
        impact = fire_impact;
        break;

    case 5:
        flying = elec_flying;
        impact = elec_impact;
        break;

    case 4:
        flying = dark_flying;
        impact = dark_impact;
        break;

    case 2:
        flying = poison_flying;
        impact = poison_impact;
        break;
    }

    direction_ = dir;
    particles_flying_.create(200, flying, &direction_, particle_shader);
    particles_flying_.setContinuous(true);
    particles_flying_.setPosition(physics_.getPosition(), true);
    particles_flying_.setPointSize(2.0f);
    particles_flying_.play();

    particles_impact_.create(2000, impact, NULL, particle_shader);
    particles_impact_.setPointSize(2.0f);
}

void Bullet::update(float dt)
{
    life_tick_ += dt;

    if (life_tick_ > life_time_)
    {
        kill();
    }

    cl::Entity3D::update(dt);
    physics_.setVelocity(8.0f * direction_);
    hitbox_.setPosition(physics_.getPosition());

    particles_flying_.updateLastPosition();
    particles_flying_.setPosition(physics_.getPosition(), false);

    particles_flying_.update(dt);
    particles_impact_.update(dt);
}

void Bullet::kill()
{
    if (alive_)
    {
        particles_flying_.off();
        particles_impact_.setPosition(physics_.getPosition(), true);
        particles_impact_.play();
    }

    alive_ = false;
}

bool Bullet::isAlive() const
{
    return alive_;
}

bool Bullet::destroy() const
{
    return !isAlive() && !particles_impact_.isPlaying(); 
}

void Bullet::setAoE(float aoe)
{
    aoe_ = aoe;
}

float Bullet::getAoE() const
{
    return aoe_;
}

cl::BulletPhysicsObject* Bullet::getPhysics()
{
    return &physics_;
}

cl::BulletTriggerObject* Bullet::getHitbox()
{
    return &hitbox_;
}

cl::ParticleEmitter* Bullet::getParticles()
{
    if (isAlive())
        return &particles_flying_;
    else
        return &particles_impact_;
}

