#pragma once
#include <Clone/3D/Entity3D.hpp>
#include <Clone/3D/Armature.hpp>
#include <Clone/Bullet/BulletCharacterObject.hpp>
#include <Clone/AssetLoader.hpp>
#include <Clone/Input.hpp>

class Ritualist : public cl::Entity3D
{ // i put braces on a new line in a class not for you but for honor
    public:
        Ritualist(glm::vec3 pos);
        ~Ritualist();

        void uploadToShader(cl::Shader *shader, float interpolation, int draw_index);
        void initialize(cl::Shader *shader);
        void update(cl::Camera *camera, float dt);

        cl::BulletCharacterObject* getPhysics();
        void die();
        bool isAlive() const;
        bool destroy() const; 
    private:
        static cl::AssetCache cache_;
        cl::ModelData *model_data_;
        cl::SkeletalAnimation anim_;
        btCapsuleShape physics_shape_;
        cl::BulletCharacterObject physics_;
        cl::SkeletalAnimation anim_idle_;
        cl::SkeletalAnimation anim_die_;
        cl::Armature armature_;
        glm::vec3 pos_;

        void input(cl::Camera *camera, float dt);
        void animations(float dt); 
};

