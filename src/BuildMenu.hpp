#pragma once
#include <Clone/Image.hpp>
#include <Clone/AssetLoader.hpp>
#include <Clone/Text.hpp>
#include <Clone/Camera.hpp>

const int NUM_TOWERS = 6;

enum TOWERS
{
    WALL = 0,
    ICE,
    POISON,
    FIRE,
    CRYSTAL,
    LIGHTNING
};

class BuildMenu
{
 public:
    BuildMenu();
    ~BuildMenu();

    void initialize(cl::Shader *image_shader);
    void update(cl::Camera *camera, float cost, const glm::vec3 &position, float dt);

    void next();
    void previous();
    void show();
    void cancel();
    TOWERS getSelection();

    bool isSelecting() const;

    cl::Image* getSelectedImage();
    cl::Text* getCostText();

 private:
    cl::Texture *tower_textures_[NUM_TOWERS];
    cl::Image tower_images_[NUM_TOWERS];
    cl::Font *cost_font_;
    cl::Text cost_text_;
    int selected_index_;
    bool selecting_;
};
