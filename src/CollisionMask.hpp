#pragma once

enum COL_MASK
{
    COL_NONE = 0x00,
    COL_ENVIRONMENT = 0x01,
    COL_PLAYER = 0x02,
    COL_ENEMY = 0x04,
    COL_BULLET = 0x08,
    COL_TOWER = 0x10
};
