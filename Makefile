BINARY_NAME=clone
INCLUDE=../clone/include/
CFLAGS=-O3 -g
FRAMEWORKS=

NO_BULLET=0

ifeq ($(NO_BULLET), 1)
	BULLET_LIBS=
	BULLET_INC=
else
	BULLET_LIBS=-lclone_bullet -lBulletDynamics -lBulletCollision -lLinearMath
endif

ifeq ($(OS),Windows_NT)

	MESSAGE="Operating System Detected: Windows"
	COMPILE=../clone/lib/windows/
	LIBS=-lclone -lmingw32 -lSDL2main -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer $(BULLET_LIBS) -lglew32 -lglu32 -lopengl32 -lminizip -lz -lm -Wall -mwindows
	BINARY=bin/windows/$(BINARY_NAME).exe

	ifeq ($(NO_BULLET), 0)
		BULLET_INC=-I"$(INCLUDE)bullet/"
	endif

else ifeq ($(shell uname), Darwin)

	MESSAGE="Operating System Detected: Mac OSX"
	COMPILE=../clone/lib/mac/
	LIBS=-clone -framework SDL2 -framework SDL2_image -framework SDL2_mixer -framework SDL2_ttf -framework SDL2_net -lminizip -lz
	FRAMEWORKS="-F../clone/lib/mac/"
	BINARY=bin/mac/$(BINARY_NAME)

else

	MESSAGE="Operating System Detected: Linux"
	COMPILE=../clone/lib/linux/
	LIBS=-lclone -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer $(BULLET_LIBS) -lGLEW -lGLU -lGL -lminizip -lz -lm -pthread -ldl -lfreetype -Wall
	BINARY=bin/linux/$(BINARY_NAME)

	ifeq ($(NO_BULLET), 0)
		BULLET_INC=-I"/usr/local/include/bullet/"
	endif

endif

srcs = $(wildcard src/*.cpp)
objs = $(srcs:.cpp=.o)
deps = $(srcs:.cpp=.d)
OBJECTS=src/*.o

all: bin

bin: $(objs)
	@echo $(MESSAGE)
	g++ $(CFLAGS) $(OBJECTS) -I"$(INCLUDE)" $(BULLET_INC) -L"$(COMPILE)" $(FRAMEWORKS) $(LIBS) -o $(BINARY)

%.o: %.cpp
	g++ $(CFLAGS) -I"$(INCLUDE)" $(BULLET_INC) -L"$(COMPILE)" -MMD -MP -Wall -c $< -o $@

clean:
	rm -f src/*.o src/*.d

-include $(deps)
